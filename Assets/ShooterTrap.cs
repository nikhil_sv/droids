﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterTrap : MonoBehaviour
{
    public GameObject bullet;
    public Transform aim;
    public float shootInterval;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        timer = shootInterval;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer -= Time.fixedDeltaTime;
        if(timer <= 0)
        {
            Shoot();
        }
               
    }

    void Shoot()
    {
        Instantiate(bullet, aim.position, aim.rotation);
        timer = shootInterval;
    }
}
