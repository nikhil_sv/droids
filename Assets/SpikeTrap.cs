﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrap : MonoBehaviour
{
    public bool isAutomated;
    public float delay;
    public float activeTime;
    public float offset;

    public bool triggered;
    public bool activated;
    public GameObject spikeSet;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        triggered = false;
        activated = false;
        if (isAutomated)
        {
            timer = offset;
            triggered = true;
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
     


        if (triggered)
        {

            timer -= Time.fixedDeltaTime;

            if (timer <= 0)
            {
                ActivateTrap();
            }
        }
        else if (activated)
        {
            timer -= Time.fixedDeltaTime;
            if (timer <= 0)
            {
                DeActivateTrap();
            }
        }
        else
        {
            spikeSet.SetActive(false);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isAutomated)
        {
            if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Enemy"))
            {
                triggered = true;
                timer = delay;
            }
        }
    }

    void ActivateTrap()
    {
        spikeSet.SetActive(true);
        //spikeSet.transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, 0, transform.position.z), Time.fixedDeltaTime);
        triggered = false;
        activated = true;
        timer = activeTime;
    }
    void DeActivateTrap()
    {
        spikeSet.SetActive(false);
        //spikeSet.transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, -2, transform.position.z), Time.fixedDeltaTime);
        activated = false;
        if(isAutomated)
        {
            timer = delay;
            triggered = true;
        }
    }
}
