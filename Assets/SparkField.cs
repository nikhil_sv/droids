﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparkField : MonoBehaviour
{
    private float sparkDamage;
    public GameObject spark;
    public GameObject sparkCaster;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            //GameObject target = other.gameObject;
            //Vector3 aim = (target.transform.position) - transform.position;
            //sparkCaster.transform.rotation = Quaternion.LookRotation(aim, Vector3.up);
            //CastSpark();



            EnemyElementEffects fxStats = other.gameObject.GetComponent<EnemyElementEffects>();

            if (!fxStats.sparkHit)
            {
                fxStats.Lightning();
                Kill();
            }

        }
    }

    public void CastSpark()
    {
        Instantiate(spark, transform.position, sparkCaster.transform.rotation);
    }

    public void Kill()
    {
        Destroy(this.gameObject);
    }
}
