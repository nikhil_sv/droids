﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UtilityStation : MonoBehaviour
{

    public int statPoints;
    public int maxHpStat;
    public int maxAmmoStat;
    public int speedStat;
    public int dmgStat;

    public Image loadBar;


    private float timer;
    private float activateTime = 1.5f;
    private MenuManager menuStats;
    private CoinManager coinStats;
    private PlayerStats playerStats;
    private PlayerController pControl;
    private UtilityPanel utilityPanel;
    // Start is called before the first frame update
    void Start()
    {
        //coinStats = FindObjectOfType<CoinManager>();
        menuStats = MainSingletons.Instance.menuManager;
       // pControl = FindObjectOfType<PlayerController>();
        utilityPanel = MainSingletons.Instance.utilityPanel;
        playerStats = MainSingletons.Instance.playerStats;

        statPoints = 5;
        maxHpStat = 1;
        maxAmmoStat = 1;
        speedStat = 1;
        dmgStat = 1;

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            menuStats.UtilityMenu(true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            SetText(playerStats.skillLevel, playerStats.maxAmmo, playerStats.maxHealth, playerStats.pControl.speed);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            menuStats.UtilityMenu(false);
        }
    }

    public void SetText(float skillPoints, float ammo,float health,float speed)
    {
        utilityPanel.skillPointsText.text = "Skill Points : " + Mathf.RoundToInt(skillPoints).ToString();
        utilityPanel.ammoText.text = Mathf.RoundToInt(ammo).ToString();
        utilityPanel.healthText.text = Mathf.RoundToInt(health).ToString();
        utilityPanel.speedText.text = Mathf.RoundToInt(speed).ToString();
    }

}
