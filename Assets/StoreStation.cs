﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreStation : MonoBehaviour
{
    public int hCost;
    public int aCost;
    public Image loadBar;

    private float timer;
    private float activateTime = 0.5f;
    private MenuManager menuStats;
    private CoinManager coinStats;
    private PlayerStats pStats;
    // Start is called before the first frame update
    void Start()
    {
        coinStats = FindObjectOfType<CoinManager>();
        menuStats = FindObjectOfType<MenuManager>();
        pStats = menuStats.player;
    }

    // Update is called once per frame
    void Update()
    {
        loadBar.fillAmount = timer / activateTime;
        if (timer >= activateTime)
        {
            Activate();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            timer += Time.fixedDeltaTime;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            timer = 0;
        }
    }
    public void Activate()
    {
        timer = 0;
        menuStats.StoreMenu();
        menuStats.GamePause();
    }

    public void AddHealth()
    {
        if (coinStats.coinsCollected >= hCost)
        {
            coinStats.coinsCollected -= hCost;
            pStats.health += 5;
            hCost *= 2;
        }
        else
        {
            Debug.Log("Not Enough Coins!!");
        }

    }

    public void AddAmmo()
    {
        if (coinStats.coinsCollected >= aCost)
        {
            coinStats.coinsCollected -= aCost;
            pStats.ammo += 10;
            aCost *= 2;

        }
        else
        {
            Debug.Log("Not Enough Coins!!");
        }

    }
}
