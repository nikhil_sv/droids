﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroidOrbiter : MonoBehaviour
{
    public float speed;
    public float followSpeed;
    public float followDistance;
    private GameObject player;
    private Rigidbody myRb;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //myRb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float pDistance = Vector3.Distance(transform.position, player.transform.position);

        transform.position = Vector3.Lerp(transform.position, player.transform.position, followSpeed * Time.fixedDeltaTime);

        //if (pDistance > followDistance)
        {
            transform.Rotate(new Vector3(0, speed * Time.fixedDeltaTime, 0));
        }

        //if(pDistance > followDistance)
        //{
        //    Vector3 dir = player.transform.position - transform.position;
        //    myRb.AddForce(dir * followSpeed * Time.fixedDeltaTime);
        //}




    }
}
