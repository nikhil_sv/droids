﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MagnetArea : MonoBehaviour
{
    public float pullForce;
    private NavMeshAgent eNav;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.GetComponent<NavMeshAgent>() != null)
        {
            eNav = other.GetComponent<NavMeshAgent>();
            Vector3 dir = transform.position - eNav.transform.position;
            eNav.velocity = dir * pullForce;
        }
        
    }
}
