﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Card", menuName = "CustomObjects/Card")]
public class EquipCard : ScriptableObject
{
    public string cardName;
    public string description;

    public Sprite cardImage;

    public int coinsCost;
}
