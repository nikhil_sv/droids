﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class isDestructible : MonoBehaviour
{
    public float HP;
    public float maxHP;
    public int hitNum;
    public bool canExplode;
    
    private Collider trigger;
    public GameObject flash;
    public Vector2 elementSpawnNum;
    public GameObject elementToSpawn;
    //public GameObject[] debris;
    // Start is called before the first frame update
    void Start()
    {
        HP = maxHP;
        trigger = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {

        if (HP <= 0)
        {
            //if (canExplode)
            //{
            //    ExplodeCrate();
            //}

            IsDestroyed();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            //TakeHit();
            ExplodeCrate();
            IsDestroyed();
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerController pStat = collision.gameObject.GetComponent<PlayerController>();
            if (pStat.inDash)
            {
                
                ExplodeCrate();
                IsDestroyed();
            }
        }
    }

    void TakeHit()
    {
        HP -= maxHP / hitNum;
    }
    void IsDestroyed()
    {
       Destroy(this.gameObject);
    }

    public void ExplodeCrate()
    {

        int spawnNum = Mathf.RoundToInt(Random.Range(elementSpawnNum.x, elementSpawnNum.y));
        for(int i = spawnNum; i > 0; i--)
        {
            Instantiate(elementToSpawn, transform.position, Quaternion.identity);
        }


        Vector3 point = transform.position;
        Instantiate(flash, point, Quaternion.identity);

    }

    
}
