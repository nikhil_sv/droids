﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public static ButtonManager Instance;

    private void Awake()
    {
        Instance = this;
    }
    public List<GameObject> buttons = new List<GameObject>();

    public void ExchangeButtons(int off, int on)
    {
        buttons[off].GetComponent<Image>().enabled = false;
        buttons[on].GetComponent<Image>().enabled = true;
    }
}
