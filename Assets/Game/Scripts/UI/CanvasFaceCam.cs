﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFaceCam : MonoBehaviour
{
    private Camera myCam;
    // Start is called before the first frame update
    void Start()
    {
        myCam = FindObjectOfType<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(transform.position + myCam.transform.rotation * Vector3.back, myCam.transform.rotation * Vector3.down);
    }
}
