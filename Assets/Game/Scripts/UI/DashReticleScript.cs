﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashReticleScript : MonoBehaviour
{
    private PlayerController pControl;
    private SpriteRenderer mySprite;
    // Start is called before the first frame update
    void Start()
    {
        pControl = FindObjectOfType<PlayerController>();
        mySprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(pControl.chargingDash)
        {
            mySprite.enabled = true;
        }
        else
        {
            mySprite.enabled = false;
        }
    }
}
