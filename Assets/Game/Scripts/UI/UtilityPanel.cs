﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UtilityPanel : MonoBehaviour
{
    public TextMeshProUGUI skillPointsText;
    public TextMeshProUGUI ammoText;
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI damageText;
    public TextMeshProUGUI speedText;

}
