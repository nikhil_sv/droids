﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerkRandomizer : MonoBehaviour
{
    public GameObject[] perks;

    private int perk1;
    private int perk2;
    private int perk3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShufflePerks()
    {
        perk1 = Random.Range(0, perks.Length);
        perk2 = Random.Range(0, perks.Length);
        perk3 = Random.Range(0, perks.Length);
        
        
        if(perk1 != perk2 && perk2 != perk3 && perk1 != perk3)
        {
            HidePerks();
            DisplayPerks(perk1, perk2, perk3);
        }
        else
        {
            ShufflePerks();
        }
        
    }

    void HidePerks()
    {
        for (int i = 0; i < perks.Length; i++)
        {
            perks[i].SetActive(false);
        }
    }
    public void DisplayPerks(int p1, int p2, int p3)
    {
        perks[p1].SetActive(true);
        perks[p2].SetActive(true);
        perks[p3].SetActive(true);

        Debug.Log(p1 + " " + p2);

    }
}
