﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PerkPanelSelector : MonoBehaviour
{
    public PerkManager perkManager;
    public int totalPerks;
    public List<string> selectedPerks = new List<string>();
    public List<PerkButton> displayedPerksButtons = new List<PerkButton>();
    int perkNumberClicked;
    public void StartPerks()
    {
        totalPerks = perkManager.perksBoolsNames.Length;
        PickThreePerks();
        DisplayPerks();
    }
    void PickThreePerks()
    {
        int j;

        while (selectedPerks.Count < 3)
        {
            j = Random.Range(0, totalPerks);
            if(!selectedPerks.Contains(perkManager.perksBoolsNames[j]))
                selectedPerks.Add(perkManager.perksBoolsNames[j]);
        }
    }

    void DisplayPerks()
    {
        for(int i = 0;i<selectedPerks.Count;i++)
        {
            displayedPerksButtons[i].text.text = selectedPerks[i];
        }
    }

    public void PerkSelected(int perkNumber)
    {
        string selectedPerkName = selectedPerks[perkNumber];

        for (int i=0;i<totalPerks;i++)
        {
            if(perkManager.perksBoolsNames[i] == selectedPerkName)
            {
                perkManager.perksBools[i] = true;
                perkManager.SetPerksVariables();
            }
        }

       // FindObjectOfType<ExitLevel>().NextLevel();
    }
}
