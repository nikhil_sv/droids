﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamagePopUp : MonoBehaviour
{
    public float lifeTime;
    public float floatSpeed;
    public Vector2 scalingRange;
    public Vector2 randomPosRange;
    public float yOffset;

    private TextMeshPro text;
    // Start is called before the first frame update
    void Start()
    {
        transform.position += new Vector3(Random.Range(randomPosRange.x, randomPosRange.y), Random.Range(randomPosRange.x, randomPosRange.y)+ yOffset, Random.Range(randomPosRange.x, randomPosRange.y));
        text = gameObject.GetComponent<TextMeshPro>();
        text.fontSize = scalingRange.x;
        //transform.localScale *= scalingRange.x;
    }
    

    // Update is called once per frame
    void Update()
    {
        lifeTime -= Time.deltaTime;
        transform.position += new Vector3(0,floatSpeed * Time.deltaTime,0);
        
        text.fontSize += scalingRange.y * Time.deltaTime;
        if(lifeTime<=0)
        {
            Destroy(this.gameObject);
        }
    }

    
}
