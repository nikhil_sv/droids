﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageDisplay : MonoBehaviour
{
    public GameObject dmgPopup;
    private TextMeshPro textMesh;

    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void DisplayDamage(float damageAmount)
    {
        GameObject popUp = Instantiate(dmgPopup, transform.position, transform.rotation);
        textMesh = popUp.GetComponent<TextMeshPro>();
        textMesh.SetText("-" + damageAmount.ToString());

    }
}
