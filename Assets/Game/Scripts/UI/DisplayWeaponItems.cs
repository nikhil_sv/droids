﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayWeaponItems : MonoBehaviour
{
    public GameObject[] weaponsList;
    public GameObject weaponCardPrefab;
    private WeaponItem weaponItem;

    private void Start()
    {
        weaponsList = Resources.LoadAll<GameObject>("Weapons");
        DisplayItems();
    }

    void DisplayItems()
    {
        GameObject tempWeaponCard;

        for(int i=0; i<weaponsList.Length; i++)
        {
            tempWeaponCard = Instantiate(weaponCardPrefab,transform,false);
            weaponItem = tempWeaponCard.GetComponent<WeaponItem>();
            weaponItem.weaponName.text = weaponsList[i].name;
            //weaponItem.weaponImage.sprite = weaponsList[i].GetComponent<Weapon>().image.sprite;
        }
    }
}
