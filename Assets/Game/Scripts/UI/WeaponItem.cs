﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WeaponItem : MonoBehaviour
{
    public Image weaponImage;
    public Text weaponName;
    public int weaponCost;


    public void EquipWeapon()
    {
        PlayerPrefs.SetString("EquippedWeapon", weaponName.text);
        SceneManager.LoadScene(1);
    }
}
