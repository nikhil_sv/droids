﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class buttonHold : MonoBehaviour, IPointerDownHandler,IPointerUpHandler
{
    public bool pointerdown;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        pointerdown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pointerdown = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
