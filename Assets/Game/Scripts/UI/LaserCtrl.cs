﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCtrl : MonoBehaviour
{
    private LineRenderer laser;
    private RaycastHit hit;
    // Start is called before the first frame update
    void Start()
    {
        laser = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            if (hit.collider)
            {
                Debug.Log(hit.transform.name);
                laser.SetPosition(1, new Vector3(0, 0, hit.distance));
            }
            else
            {
                laser.SetPosition(1, new Vector3(0, 0, 100));
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(0, 0, hit.distance));

    }
}
