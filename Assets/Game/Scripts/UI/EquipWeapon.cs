﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipWeapon : MonoBehaviour
{
    GameObject lootRoom;
    WeaponPickup weaponPickup;
    public PlayerSingelton playerSingelton;

    public void CallSwitchWeapon()
    {
        weaponPickup = playerSingelton.weaponInSight;
        if (weaponPickup != null)
        {
            weaponPickup.SwitchWeapon();
        }
    }
}
