﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GroupControl : MonoBehaviour
{

    private CinemachineTargetGroup tGroup;
    //public Enemy[] enemies;
    public CinemachineVirtualCamera groupCam;
    public bool groupMode;

    //private EnemyManager eManager;
    private WeaponSwitch aimStat;
    private GameObject activeGun;
    // Start is called before the first frame update
    void Start()
    {

        tGroup = gameObject.GetComponent<CinemachineTargetGroup>();
       // eManager = FindObjectOfType<EnemyManager>();
        aimStat = FindObjectOfType<WeaponSwitch>();

    }

    // Update is called once per frame
    void Update()
    {
        
        bool aiming = aimStat.activeWeapon.GetComponentInChildren<AutoAim>().targeting;
        if(aiming)
        {
            groupMode = true;
            groupCam.Priority = 11;
        }
        else
        {
            groupMode = false;
            groupCam.Priority = 9;
        }
        //enemies = eManager.enemies;
        //if (eManager.closetEnemy != null)
        //{
        //    if (tGroup.m_Targets.Length <= 2)
        //    {
        //        tGroup.AddMember(eManager.closetEnemy.transform, 1, 60);
                
        //    }
        //}
        //enemies = FindObjectsOfType<Enemy>();
        //foreach (Enemy enemy in enemies)
        //{
        //    if (tGroup.m_Targets.Length <= enemies.Length)
        //    {
        //        tGroup.AddMember(enemy.gameObject.transform, 1, 60);

        //    }

        //}
        //if (enemies.Length > 1)
        //{
        //    groupMode = true;
        //    groupCam.Priority = 11;
        //}
        //else
        //{
        //    groupMode = false;
        //    groupCam.Priority = 9;
        //}

    }
}
