﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTurret : MonoBehaviour
{
    public GameObject gun;
    public GameObject target;
    public GameObject bullet;
    public Collider fov;

    public bool targeting;
    public bool shooting;
    public bool inCooldown;
    public Transform aimPoint;
    public float shotInterval;
    public float cooldownTime;
    public float shotsNum;
    public Vector2 shootTimeRange;

    public Vector3 aimOff;

    private EnemyStats eStats;
    private float timer;
    private float shot;
    // Start is called before the first frame update
    void Start()
    {
        targeting = false;
        eStats = GetComponent<EnemyStats>();
        shot = shotsNum;
    }

    // Update is called once per frame
    void Update()
    {
        if (targeting && !eStats.stunned)
        {
            eStats.attackChargeBar.fillAmount = timer / shotInterval;
            Vector3 aim = (target.transform.position + aimOff) - gun.transform.position;
            gun.transform.rotation = Quaternion.LookRotation(aim, Vector3.up);
            gun.transform.rotation = new Quaternion(0, gun.transform.rotation.y, 0, gun.transform.rotation.w);

            timer += Time.deltaTime;
            //print(timer);

            if (shot > 0)
            {
                if (timer > shotInterval)
                {
                    Shoot();
                    timer = 0;
                    shooting = true;
                }
            }
            else
            {
                CoolDown();
            }
            if(inCooldown)
            {
                if(timer > cooldownTime)
                {
                    inCooldown = false;
                    shot = shotsNum;
                }
            }

        }
    }

    private void OnTriggerStay(Collider fov)
    {
        if (fov.gameObject.CompareTag("Player"))
        {
            targeting = true;
            target = fov.gameObject;
        }

    }
    private void OnTriggerExit(Collider fov)
    {
        if (fov.gameObject.CompareTag("Player"))
        {
            targeting = false;

        }
    }

    void Shoot()
    {
        Instantiate(bullet, aimPoint.transform.position, aimPoint.transform.rotation);
        shooting = false;
        shotInterval = Random.Range(shootTimeRange.x, shootTimeRange.y);
        shot -= 1;
    }

    void CoolDown()
    {
        inCooldown = true;
    }
}
