﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyElementEffects : MonoBehaviour
{
    public bool isBurning;
    public bool isCorroding;
    public bool sparkHit;
    public float burnRate;
    public float effectDuration;
    public float fireDamage;
    public float sparkDamage;
    public float freezeTime;
    public float effeciency;                 // Percentage boost to damage taken from elements;

    public GameObject sparkField;
    public GameObject burningFX;
    public GameObject acidFX;
    public GameObject sparkFX;

    private float burnTimer;
    private float sparkHitTimer;
    private float corrodeTimer;
    private EnemyStats eStats;
    private float dmgResist;
    // Start is called before the first frame update
    void Start()
    {
        eStats = GetComponent<EnemyStats>();
        burnTimer = 0;
        dmgResist = eStats.dmgResist;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isBurning)
        {
            burnTimer -= Time.fixedDeltaTime;
            burningFX.SetActive(true);
        }

        if (burnTimer <= 0)
        {
            burnTimer = 0;
            isBurning = false;
            StopCoroutine(BurnDamage());
            burningFX.SetActive(false);
        }


        if (isCorroding)
        {
            corrodeTimer -= Time.fixedDeltaTime;
            eStats.acid = true;
            acidFX.SetActive(true);
            //eStats.dmgResist = 0;
        }

        if (corrodeTimer <= 0)
        {
            corrodeTimer = 0;
            isCorroding = false;
            eStats.acid = false;
            //eStats.dmgResist = dmgResist;
            acidFX.SetActive(false);
        }

        if (sparkHit)
        {
            sparkHitTimer -= Time.fixedDeltaTime;
            if (sparkHitTimer <= 0)
            {
                sparkHit = false;
            }
        }

    }

    public void Burn()
    {
        burnTimer += effectDuration;
        StartCoroutine(BurnDamage());
        isBurning = true;
    }
    public void Acid()
    {
        corrodeTimer += effectDuration * (1 + effeciency / 100);
        isCorroding = true;
    }

    public void Freeze()
    {
        eStats.Stun(freezeTime * (1 + effeciency / 100));
    }

    public void Lightning()
    {
        if (!sparkHit)
        {
            sparkHitTimer = 0.8f;                                         // Cooldown timer to get hit by spark again
            eStats.Hurt(sparkDamage * (1 + effeciency / 100));
            eStats.Stun(0.3f);                                            // Mini Stun Time;

            Instantiate(sparkFX, transform.position, Quaternion.identity);
            ChainLightning();
            sparkHit = true;
        }
    }
    public void ChainLightning()
    {
        Instantiate(sparkField, transform.position, transform.rotation);
    }



    IEnumerator BurnDamage()
    {
        while (burnTimer > 0)
        {
            eStats.Hurt(fireDamage * (1 + effeciency/100));
            yield return new WaitForSeconds(burnRate);
        }
        yield return null;

    }



}
