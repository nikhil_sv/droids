﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCatalog : MonoBehaviour
{
    //singelton
    public static EnemyCatalog Instance;
    private void Awake()
    {
        Instance = this;
    }

    //spawnManager
    public enum EnemyTypes
    {
        Melee,
        Range,
        Type3,
        Type4
    }

    [System.Serializable]
    public class EnemiesCatalog
    {
        public EnemyTypes enemyTypes;
        public List<GameObject> enemyPrefabs = new List<GameObject>();
    }
    [Space]
    [Header("Enemy Catalog")]
    [Tooltip("Drop in Prefabs to match the type on Enemy")]
    public List<EnemiesCatalog> enemiesCatalogs = new List<EnemiesCatalog>();
}
