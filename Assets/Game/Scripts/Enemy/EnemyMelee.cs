﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMelee : MonoBehaviour
{
    private NavMeshAgent myAgent;
    public float timer;
    private GameObject target;

    public bool targeting;
    public bool isMoving;
    public bool isAttacking;
    public float patrolTimer;
    public float attackDMG;
    public float attackRange;
    public Collider trigger;
    public Animator animControl;

    private Rigidbody myRb;
    // Start is called before the first frame update
    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(timer>0)
        {
            timer -= Time.fixedDeltaTime;
        }
        if (target != null)
        {
            float tracking = Vector3.Distance(transform.position, target.transform.position);

            if(tracking>attackRange && !isMoving)
            {
                Attack();
            }
            else
            {
                isAttacking = false;
            }
        }
        if(timer<=0 && !isAttacking)                                         // Patrol Timer
        {
            Patrol();
        }

        if(targeting)
        {
            ChasePlayer();
        }

        if(myAgent.velocity != Vector3.zero)                 // Check if Velocity is zero, go to Idle
        {
            isMoving = true;                                  //Move to Position
            Debug.Log(myAgent.speed);
        }
        else
        {
            isMoving = false;
            myAgent.isStopped = true;
            myAgent.SetDestination(transform.position);
        }
        

        if(isMoving)                                                // Set Move Animation State
        {
            myAgent.isStopped = false;
            animControl.SetBool("Moving", true);
        }
        else if(isAttacking)
        {
            myAgent.isStopped = false;
            animControl.SetBool("Attacking", true);
        }
        else
        {
            animControl.SetBool("Moving", false);
            animControl.SetBool("Attacking", false);
        }
        
    }




    private void OnTriggerEnter(Collider trigger)
    {
        if (trigger.gameObject.CompareTag("Player"))
        {
            targeting = true;
            target = trigger.gameObject;
        }
    }

    private void OnTriggerExit(Collider trigger)
    {
        if (trigger.gameObject.CompareTag("Player"))
        {
            targeting = false;
            target = null;
        }
    }

    public void Patrol()
    {
        myAgent.isStopped = false;
        Vector3 randomPos = transform.position + new Vector3(Random.Range(-50, 50), Random.Range(-50, 50), Random.Range(-50, 50));
        myAgent.SetDestination(randomPos);
        timer = patrolTimer;
    }




    public void Hurt()
    {
        myAgent.isStopped = true;
        animControl.SetTrigger("Hurt");

    }

    public void attackDash()
    {
        myRb.velocity = transform.forward * 500;
        Debug.Log("Attack");
    }
    public void ChasePlayer()
    {
        myAgent.SetDestination(target.transform.position);
    }

    public void Attack()
    {
        isAttacking = true;
    }

    
}
