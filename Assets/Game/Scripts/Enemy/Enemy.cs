﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [HideInInspector]
    public bool overlapping = false;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Obstacles")
        {
            overlapping = true;
            print("OverLap");
        }
    }

    public bool CheckOverlap()
    {
        return overlapping;
    }
}
