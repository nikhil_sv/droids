﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class EnemyManager : MonoBehaviour
{
    AutoAim[] autoAims;
    public List<GameObject> enemiesList = new List<GameObject>();
    public GameObject closetEnemy;
    public Enemy[] enemies;
    public float maxRange = 1000;
    public int enemiesKilled = 0;

    // Start is called before the first frame update
    void Start()
    {
        //autoAims = GetComponentsInChildren<AutoAim>();
    }

    // Update is called once per frame
    void Update()
    {
        //ClosestEnemy();
    }

    public void PopulateEnemyList()
    {
        enemies = FindObjectsOfType<Enemy>();
        foreach (Enemy enemy in enemies)
        {
            if(!enemiesList.Contains(enemy.gameObject))
                enemiesList.Add(enemy.gameObject);
        }
    }

    void ClosestEnemy()
    {
        float range = maxRange;
        foreach (GameObject enemyGO in enemiesList)
        {
            if (enemyGO != null)
            {
                float distance = Vector3.Distance(enemyGO.transform.position, transform.position);
                if (distance < range)
                {
                    range = distance;
                    closetEnemy = enemyGO;
                }
            }
        }

        //foreach (AutoAim autoAim in autoAims)
        //{
        //    autoAim.enemy = closetEnemy;
        //}
    }

}
