﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //singelton
    public static EnemySpawner Instance;
    private void Awake()
    {
        Instance = this;
    }
    public GameObject spawnPointPrefab;
    [System.Serializable]
    public class SpawnManager
    {
        public int wave;
        public List<EnemyCatalog.EnemyTypes> enemyTypesss = new List<EnemyCatalog.EnemyTypes>();
    }

    [Space]
    [Header("Spawn Manager")]
    public List<Transform> spawnPoints = new List<Transform>();
    private List<Transform> spawnPointsTemp = new List<Transform>();
    public List<SpawnManager> enemySpawnWaves = new List<SpawnManager>();
    [Space]
    [HideInInspector]
    public Transform center;
    public Vector3 spawnAreaSize;

    public Vector3 side;
    public float radius = 4f;
    public Collider[] colliders;
    public LayerMask layerMask;
    public int totalEnemies = 0;
    public bool roomCompleted = false;
    int waveNumber;
    EnemyRoomManager enemyRoomManager;
    private void Start()
    {
        center = transform.parent;
    }
    void ResetList()
    {
        spawnPointsTemp.Clear();
        spawnPointsTemp.AddRange(spawnPoints);
    }

    public void SpawnWaves()
    {
        waveNumber = 0;
        InvokeRepeating("SpawnEnemy", 0f, 8f);
    }
    public void SpawnEnemy()
    {
        GameObject enemy;
        for (int i = 0; i < enemySpawnWaves[waveNumber].enemyTypesss.Count; i++)
        {
            #region Using SpawnPoints
            if (spawnPointsTemp.Count == 0)
                ResetList();
            int index = Random.Range(0, spawnPointsTemp.Count);
            Transform spawnPosition = spawnPointsTemp[index];
            spawnPointsTemp[index] = spawnPointsTemp[spawnPointsTemp.Count - 1];
            spawnPointsTemp.RemoveAt(spawnPointsTemp.Count - 1);

            int count = EnemyCatalog.Instance.enemiesCatalogs[0].enemyPrefabs.Count;
            // next line instead of enemiesCatalogs[0] it should be the enemySpawnWaves[waveNumber].enemyTypess
            
            foreach(EnemyCatalog.EnemiesCatalog ec in EnemyCatalog.Instance.enemiesCatalogs)
            {
                if(enemySpawnWaves[waveNumber].enemyTypesss[i] == ec.enemyTypes)
                {
                   enemy = Instantiate(ec.enemyPrefabs[Random.Range(0, count)], spawnPosition.position, Quaternion.identity, center) as GameObject;
                   enemy.GetComponent<EnemyStats>().roomSpawnedIn = center.gameObject;
                }
            }
            //enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().Warp(spawnPosition.position);
            enemyRoomManager = center.GetComponent<EnemyRoomManager>();
            totalEnemies++;
            #endregion

            #region Using Overlap Sphere
            //Vector3 pos = center.position + new Vector3(Random.Range(-spawnAreaSize.x / 2, spawnAreaSize.x / 2), Random.Range(-spawnAreaSize.y / 2, spawnAreaSize.y / 2), Random.Range(-spawnAreaSize.z / 2, spawnAreaSize.z / 2));
            ////colliders = Physics.OverlapSphere(pos, radius, layerMask.value);
            ////print(colliders.Length);
            ////if (colliders.Length == 0)
            ////{
            //int count = EnemyCatalog.Instance.enemiesCatalogs[0].enemyPrefabs.Count;
            //enemy = Instantiate(EnemyCatalog.Instance.enemiesCatalogs[0].enemyPrefabs[Random.Range(0, count)], pos, Quaternion.identity,center);
            //if(enemy.GetComponent<Enemy>().CheckOverlap())
            //{
            //    enemy.transform.position = center.position;
            //}
            ////}
            //else
            //    print("Collided");
            #endregion
        }
        MainSingletons.Instance.enemyManager.PopulateEnemyList();
        ++waveNumber;
        if(waveNumber == enemySpawnWaves.Count)
        {
            CancelInvoke();
        }
    }

    void SpawnPoints()
    {
        GameObject spawnPoint;
        do
        {
            Vector3 pos = center.position + new Vector3(Random.Range(-spawnAreaSize.x / 2, spawnAreaSize.x / 2), Random.Range(-spawnAreaSize.y / 2, spawnAreaSize.y / 2), Random.Range(-spawnAreaSize.z / 2, spawnAreaSize.z / 2));
            spawnPoint = Instantiate(spawnPointPrefab, pos, Quaternion.identity, center);
            print(pos);
        } while (!spawnPoint.GetComponent<SpawnPoint>().overlapPoint);

    }
    public bool CheckRoomCompletion()
    {
        if (enemyRoomManager != null)
        {
            if ((Mathf.Abs(enemyRoomManager.enemiesKilled) == totalEnemies) && totalEnemies != 0)
            {
                roomCompleted = true;

                return true;
            }
        }
            return false;
    }


    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireCube(transform.position, side);
    //}
}



















