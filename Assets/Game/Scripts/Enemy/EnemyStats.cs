﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using Cinemachine;
using TMPro;


public class EnemyStats : MonoBehaviour
{
    public float Health;
    public float maxHealth;
    public float dmgResist;
    public bool stunned;
    public bool acid;

    //public float Dmg;
    public Vector2 rewardNum;
    public Image healthBar;
    public Image attackChargeBar;
    public TextMeshProUGUI killReticle;
    public TextMeshProUGUI stunReticle;
    //public Text dmgText;
    public GameObject[] reward;
    public EnemyManager enemyManager;
    public GameObject roomSpawnedIn;

    public GameObject deathVfx;
    public GameObject dashHitVfx;

    private CinemachineTargetGroup camTargetGroup;
    private float timer;
    //public Animator myAnim;

    private DamageDisplay dmgDisplay;
    private EnemyMeleeChaser attackStats;
    private PlayerXpManager xpStats;
    private PlayerStats pStats;
    private NavMeshAgent myNav;
    
    
    // Start is called before the first frame update
    void Start()
    {
        enemyManager = FindObjectOfType<EnemyManager>();
        camTargetGroup = FindObjectOfType<CinemachineTargetGroup>();
        xpStats = FindObjectOfType<PlayerXpManager>();
        pStats = FindObjectOfType<PlayerStats>();
        dmgDisplay = gameObject.GetComponent<DamageDisplay>();
        attackStats = GetComponent<EnemyMeleeChaser>();
        myNav = GetComponent<NavMeshAgent>();

        maxHealth = maxHealth + ((20*maxHealth/100)* pStats.skillLevel);               // 20% increase in Health every skillLevel
        Health = maxHealth;
        healthBar.color = Color.red;
        killReticle.enabled = false;
        stunReticle.enabled = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        healthBar.fillAmount = Health / maxHealth;
        if (attackStats != null)
        {
            attackChargeBar.fillAmount = attackStats.attackCharge / attackStats.attackChargeTime;
        }

        if (Health <= maxHealth / 4)
        {
            healthBar.color = Color.yellow;
            killReticle.enabled = true;
        }

        if (Health <= 0)
        {

            rewardBurst();
            enemyManager.enemiesList.Remove(this.gameObject);              // Remove from Enemy Manager List
            //xpStats.addCombo();
            pStats.AddSkillPoints(20);
            camTargetGroup.RemoveMember(this.transform);                   // Remove from GroupTarget List

            Destroy(this.gameObject);
            roomSpawnedIn.GetComponent<EnemyRoomManager>().enemiesKilled--;

        }

        if (stunned)
        {
            timer -= Time.fixedDeltaTime;
            stunReticle.enabled = true;
            if (timer <= 0)
            {
                timer = 0;
                stunned = false;
                stunReticle.enabled = false;
            }
            myNav.velocity = Vector3.zero;
            myNav.isStopped = true;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            bullet bulletStat = collision.gameObject.GetComponent<bullet>();
            if (bulletStat.isCrit)
            {

                Hurt(pStats.activeWeapon.critDamage);
            }
            else
            {
                Hurt(pStats.activeWeapon.damage);                                              // Take Damage stored inside bullet
            }

            //dmgText.text = bulletStat.damage.ToString();     // Damage Indicator
            //Rigidbody myRb = GetComponent<Rigidbody>();
            //myRb.AddForce(-transform.forward * 10, ForceMode.Impulse);
        }





        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerController dashStat = collision.gameObject.GetComponent<PlayerController>();
            if (dashStat.inDash)
            {

                Hurt(pStats.dashDamage);                                                                           // Run Hurt Function
                ContactPoint contact = collision.contacts[0];
                Vector3 dir = contact.point - transform.position;
                HurtKnockBack(150, dir);



                if (Health <= maxHealth / 4)
                {

                    TimeManager.Instance.KillTime();                                                    // Trigger Slo-mo

                    PlayerController pControl = FindObjectOfType<PlayerController>();                   // Reset Player Dash Cooldown to chain Dash Attacks
                    pControl.ResetCooldown();
                    Health = 0;

                }
                Instantiate(dashHitVfx, transform.position, Quaternion.identity);
            }

        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Orbiter"))
        {
            Hurt(10);
            //Stunned();
        }

    }



    public void rewardBurst()
    {
        float num = Random.Range(Mathf.FloorToInt(rewardNum.x), Mathf.FloorToInt(rewardNum.y));
        //Debug.Log("Rewarded " + num + " coins");
        for (int i = 0; i < num; i++)
        {

            GameObject coin = Instantiate(reward[Random.Range(0, reward.Length)], transform.position, Quaternion.identity);
            coin.GetComponent<Rigidbody>().velocity = new Vector3(Random.Range(-5, 5), 1, Random.Range(-5, 5)) * 5;
        }

        Instantiate(deathVfx, transform.position, Quaternion.identity);                                  // Death VFX
    }

    public void Hurt(float damage)
    {
        //float effectiveDamage = (damage - (damage * dmgResist / 100));             // Resist Damage taken based on resistance
        if (acid)
        {
            Health -= 2 *damage;
            dmgDisplay.DisplayDamage(2 * damage);
        }
        else
        {
            Health -= damage;
            dmgDisplay.DisplayDamage(damage);
        }
        //myAnim.SetTrigger("Hurt");
    }

    public void HurtKnockBack(float force, Vector3 dir)
    {
        //Rigidbody myRb = GetComponent<Rigidbody>();
        //myRb.AddForce(-transform.forward * force, ForceMode.VelocityChange);
        myNav.isStopped = true;
        myNav.velocity = Vector3.zero;
        //myNav.velocity += -transform.forward * force;
        myNav.velocity += -dir.normalized * force;
    }

    public void Stun(float stunTime)
    {
        //Debug.Log("STUNNED");
        stunned = true;
        timer = stunTime;
    }

}
