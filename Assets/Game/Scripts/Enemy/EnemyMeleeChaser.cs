﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMeleeChaser : MonoBehaviour
{
    public float moveSpeed;
    public float range;
    public float attackDMG;
    public float attackZoneTime;
    public float attackChargeTime;
    public float dashForce;
    public bool dumbChaser;


    public Collider vision;

    public GameObject target;
    //private Animator myAnim;
    private NavMeshAgent myNav;
    private GameObject pDamage;
    private EnemyStats myEStats;
    private bool targeting;
    
    private float actionTimer;
    private float dashTimer;
    public float attackCharge;

    // Start is called before the first frame update
    void Start()
    {
        myNav = GetComponent<NavMeshAgent>();
        pDamage = GetComponentInChildren<PlayerDamage>().gameObject;
        myEStats = GetComponent<EnemyStats>();
        pDamage.SetActive(false);
        myNav.Warp(transform.position);
        //myAnim = GetComponentInChildren<Animator>();
        myNav.speed = moveSpeed;
        attackCharge = 0;
        myNav.isStopped = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!myEStats.stunned)
        {
            actionTimer -= Time.fixedDeltaTime;
        }
        
        //Debug.Log(actionTimer);


        if (actionTimer <= 0)
        {
            actionTimer = 0;
        }

        if (!dumbChaser)
        {

            if (targeting && actionTimer <= 0)                                                    // Check if Player in Sight
            {
                float dist = Vector3.Distance(transform.position, target.transform.position);

                if (dist < range)
                {

                    //actionTimer = 0;
                    attackCharge += Time.fixedDeltaTime;
                    if (attackCharge > 0 && attackCharge < attackChargeTime)
                    {
                        myNav.velocity /= 2;
                    }
                    if (attackCharge > attackChargeTime)
                    {
                        ResetSpeed();
                        EnemyAttack();
                    }
                }
                else if(!targeting)
                {
                    EnemyPatrol();
                }
                else
                {
                    attackCharge = 0;
                    ResetSpeed();
                    EnemyMove(1);
                    
                }
            }
        }
        else
        {
            if (targeting && actionTimer <= 0)
            {
                pDamage.SetActive(true);
                EnemyMove(1);
            }
        }


        if (target == null && actionTimer <= 0)                         // Patrol
        {
            EnemyPatrol();
        }

        if (myNav.velocity != Vector3.zero)                           // Check if Moving
        {
            //myAnim.SetBool("Moving", true);
            //myAnim.SetBool("Attacking", false);
        }
        else
        {
            //myAnim.SetBool("Moving", false);
            //myNav.isStopped = true;
        }
    }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    private void OnTriggerEnter(Collider vision)
    {
        if (vision.gameObject.CompareTag("Player"))
        {
            target = vision.gameObject;
            targeting = true;
        }
    }


    public void EnemyMove(float timeCost)
    {
        myNav.isStopped = false;
        myNav.SetDestination(target.transform.position);
        //myNav.Move((target.transform.position - transform.position) * moveSpeed*Time.deltaTime);
        actionTimer = timeCost;
    }

    public void EnemyAttack()
    {

        //myAnim.SetBool("Attacking", true);
        pDamage.SetActive(true);

        {
            EnemyDash();

            attackCharge = 0;
            actionTimer = 2;
            
        }


    }

    public void EnemyPatrol()
    {
        Vector3 newPos = RandomNavSphere(transform.position, 50, -1);
        myNav.SetDestination(newPos);
        actionTimer = Random.Range(2, 5);
    }

    public void EnemyDash()
    {
        myNav.velocity = Vector3.zero;
        transform.LookAt(target.transform.position);
        //myNav.Move(transform.forward* dashForce);
        myNav.velocity += transform.forward * dashForce;
        
       
        //myNav.Warp((target.transform.position-transform.position)* dashForce);
    }
    public void EnemyKnockback()
    {
        myNav.velocity += -transform.forward * dashForce;
    }

    public void ResetSpeed()
    {
        myNav.speed = moveSpeed;
    }


}
