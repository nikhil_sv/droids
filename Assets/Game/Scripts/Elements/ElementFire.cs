﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementFire : MonoBehaviour
{
    private Rigidbody myRb;
    public Vector2 range;
    public Vector2 killTime;
    // Start is called before the first frame update
    void Start()
    {
        myRb = gameObject.GetComponent<Rigidbody>();
        myRb.isKinematic = false;
        Vector3 pos = new Vector3(Random.Range(range.x, range.y), 1, Random.Range(range.x, range.y));
        myRb.AddForce(pos * 5, ForceMode.Impulse);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ground") && myRb != null)
        {
            myRb.isKinematic = true;
            Destroy(this.gameObject, Random.Range(killTime.x, killTime.y));
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            EnemyElementEffects fxStats = other.gameObject.GetComponent<EnemyElementEffects>();
            fxStats.Burn();
        }

    }
}
