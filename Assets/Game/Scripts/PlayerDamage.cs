﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour
{
    public EnemyMeleeChaser aStats;
    

    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(this.gameObject.activeSelf)
        {
            timer += Time.fixedDeltaTime;
        }
        if(timer >= aStats.attackZoneTime)
        {
            this.gameObject.SetActive(false);
            timer = 0;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            PlayerStats pStats = other.GetComponent<PlayerStats>();
            pStats.TakeDamage(aStats.attackDMG);
        }
    }
}
