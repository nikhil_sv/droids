﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootCrate : MonoBehaviour
{
    public Image chestBar;
    public GameObject[] reward;
    public int rewardNum;

    private float timer;
    private float openTime;

    // Start is called before the first frame update
    void Start()
    {
        openTime = 0.8f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        chestBar.fillAmount = timer / openTime;
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Player"))
    //    {
    //        PlayerController pStat = collision.gameObject.GetComponent<PlayerController>();
    //        if (pStat.inDash)
    //        {
    //            RewardBurst();
    //            Destroy(this.gameObject);
    //        }
    //    }

    //    if (collision.gameObject.CompareTag("Bullet"))
    //    {
    //        PlayerController pStat = collision.gameObject.GetComponent<PlayerController>();
    //        if (pStat.inDash)
    //        {
    //            RewardBurst();
    //            Destroy(this.gameObject);
    //        }
    //    }

    //}

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            timer += Time.fixedDeltaTime;

            if(timer >= openTime)
            {
                RewardBurst();
                Destroy(this.gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            timer = 0;
        }
    }

    void RewardBurst()
    {
        for (int i = 0; i < rewardNum; i++)
        {
            GameObject coin = Instantiate(reward[Random.Range(0, reward.Length)], transform.position, Quaternion.identity);
            //coin.GetComponent<Rigidbody>().velocity = new Vector3(Random.Range(-5, 5), 2, Random.Range(-5, 5)) * 5;
        }
    }

}
