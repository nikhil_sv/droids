﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootBullet : MonoBehaviour
{

    public GameObject bullet;
    public GameObject critBullet;
    public GameObject barrier;
    public GameObject power;
    public GameObject mainAimPoint;
    public GameObject doubleAimPoint1;
    public GameObject doubleAimPoint2;
    public GameObject diagAimPoint1;
    public GameObject diagAimPoint2;
    public PlayerStats stats;

    private bool doubleBullet;
    private bool diagonalBullet;
    private bool isFireBullet;
    private bool isIceBullet;
    private bool isAcidBullet;
    private bool isSparkBullet;

    private WeaponShootStats wStat;
    private PerkManager perkManager;
    private GameObject player;

    public float recoilForce;


    // Start is called before the first frame update
    void Start()
    {
        mainAimPoint = GameObject.Find("Aim");
        stats = GameObject.Find("PlayerBall").GetComponent<PlayerStats>();
        wStat = gameObject.GetComponent<WeaponShootStats>();
        perkManager = MainSingletons.Instance.perkManager;
        doubleBullet = perkManager.doubleBullet;
        diagonalBullet = perkManager.diagonalBullet;
        isFireBullet = perkManager.fireBullet;
        isIceBullet = perkManager.iceBullet;
        isAcidBullet = perkManager.acidBullet;
        isSparkBullet = perkManager.sparkBullet;

        player = GameObject.FindGameObjectWithTag("Player");
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        AutoAim aimStat = GetComponent<AutoAim>();
    }

    public void Shoot()
    {
        if(perkManager.fireBullet)
        {
            bullet = perkManager.fireBulletGO;
        }
        if (perkManager.iceBullet)
        {
            bullet = perkManager.iceBulletGO;
        }
        CameraShakeControl camera = FindObjectOfType<CameraShakeControl>();
        camera.gunShake();

        if (!perkManager.doubleBullet)
        {
            Instantiate(bullet, mainAimPoint.transform.position, mainAimPoint.transform.rotation);
        }
        else
        {
            Instantiate(bullet, doubleAimPoint1.transform.position, doubleAimPoint1.transform.rotation);
            Instantiate(bullet, doubleAimPoint2.transform.position, doubleAimPoint2.transform.rotation);
        }

        if (perkManager.diagonalBullet)
        {
            Instantiate(bullet, diagAimPoint1.transform.position, diagAimPoint1.transform.rotation);
            Instantiate(bullet, diagAimPoint2.transform.position, diagAimPoint2.transform.rotation);
        }


        //bulletShot.GetComponent<bullet>().damage = wStat.damage;

        //stats.ammo -= bullet.GetComponent<bullet>().ammoCost;
        //time += 1;
       //GameObject player = GameObject.FindGameObjectWithTag("Player");
        Rigidbody playerrRB = player.GetComponent<Rigidbody>();
        playerrRB.AddForce(-transform.forward * recoilForce, ForceMode.Impulse);
        //playerrRB.velocity = Vector3.zero;
        //playerrRB.velocity /= 2;



    }

    public void CritShoot()
    {
        if (isIceBullet && isFireBullet)
        {
            float randomNum = Random.Range(0, 2);
            Debug.Log(randomNum);
            if (randomNum == 0)
            {
                critBullet = perkManager.fireBulletGO;
            }
            if (randomNum == 1)
            {
                critBullet = perkManager.iceBulletGO;
            }
        }
        else if (isFireBullet && isAcidBullet)
        {
            float randomNum = Random.Range(0, 2);
            Debug.Log(randomNum);
            if (randomNum == 0)
            {
                critBullet = perkManager.fireBulletGO;
            }
            if (randomNum == 1)
            {
                critBullet = perkManager.acidBulletGO;
            }
        }
        else
        {
            if (isFireBullet)
            {
                critBullet = perkManager.fireBulletGO;
            }
            if (isIceBullet)
            {
                critBullet = perkManager.iceBulletGO;
            }
            if (isAcidBullet)
            {
                critBullet = perkManager.acidBulletGO;
            }
            if (isSparkBullet)
            {
                critBullet = perkManager.sparkBulletGO;
            }
        }


        CameraShakeControl camera = FindObjectOfType<CameraShakeControl>();
        camera.gunShake();

        if (!perkManager.doubleBullet)
        {
            Instantiate(critBullet, mainAimPoint.transform.position, mainAimPoint.transform.rotation);
        }
        else
        {
            Instantiate(critBullet, doubleAimPoint1.transform.position, doubleAimPoint1.transform.rotation);
            Instantiate(critBullet, doubleAimPoint2.transform.position, doubleAimPoint2.transform.rotation);
        }

        if (perkManager.diagonalBullet)
        {
            Instantiate(critBullet, diagAimPoint1.transform.position, diagAimPoint1.transform.rotation);
            Instantiate(critBullet, diagAimPoint2.transform.position, diagAimPoint2.transform.rotation);
        }


        //stats.ammo -= bullet.GetComponent<bullet>().ammoCost;
        //time += 1;
        //GameObject player = GameObject.FindGameObjectWithTag("Player");
        Rigidbody playerrRB = player.GetComponent<Rigidbody>();
        playerrRB.AddForce(-transform.forward * recoilForce, ForceMode.VelocityChange);
        //playerrRB.velocity = Vector3.zero;
        //playerrRB.velocity /= 2;

    }

    //public void ShootPower()
    //{
    //    if (stats.charge >= stats.maxCharge)
    //    {
    //        //Instantiate(power, turret.transform.position, turret.transform.rotation);
    //        Instantiate(barrier, turret.transform.position, turret.transform.rotation);
    //        stats.charge = 0;

    //    }
    //}
}
