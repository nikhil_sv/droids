﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAim : MonoBehaviour
{
    public GameObject enemy;
    public GameObject fovStartPoint;
    public GameObject reticle;
    Vector3 targetDirection;
    public float rotationSpeed;
    public float maxAngle;
    private float targetDistance;
    public float aimDistance;

    private Fov fov;
    private Quaternion targetRotation;
    private Quaternion lookAt;

    public bool targeting;


    void FixedUpdate()
    {
        #region commented 
        ////if (transform.parent.GetComponentInParent<EnemyManager>() != null)
        ////{
        ////    enemy = transform.parent.GetComponentInParent<EnemyManager>().closetEnemy;
        ////}
        ////if (GetComponent<WeaponPickup>().weaponSwitch != null)
        ////{
        ////    fovStartPoint = GetComponent<WeaponPickup>().weaponSwitch.gameObject;
        ////}
        ////PlayerController body = FindObjectOfType<PlayerController>();
        //enemy = GameObject.FindGameObjectWithTag("Enemy");
        ////if (enemy != null)
        ////    targetDistance = Vector3.Distance(enemy.transform.position, transform.position);
        //print("Distance to target: " + targetDistance);


        ////if (EnemyInFOV(fovStartPoint))
        ////{
        ////    if (targetDistance <= aimDistance)                        // MaxDistance check
        ////    {
        ////        Vector3 direction = enemy.transform.position - transform.position;
        ////        targetRotation = Quaternion.LookRotation(direction);
        ////        lookAt = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.fixedDeltaTime * rotationSpeed);
        ////        //transform.rotation = lookAt;
        ////        transform.rotation = new Quaternion(0, lookAt.y, 0, lookAt.w);
        ////        targeting = true;
        ////    }
        ////    else
        ////    {
        ////        targeting = false;
        ////        enemy = null;
        ////    }
        ////}
        ////else
        ////{

        ////    //targetRotation = Quaternion.Euler(0, 0, 0);
        ////    //transform.localRotation = Quaternion.RotateTowards(transform.localRotation, targetRotation, Time.deltaTime * rotationSpeed);
        ////    //transform.localRotation = Quaternion.RotateTowards(transform.localRotation, body.droidBody.transform.localRotation, Time.fixedDeltaTime * rotationSpeed);
        ////    transform.localRotation = Quaternion.Euler(0, 0, 0);                                                              // Rotate Gun when not targeting

        ////    targeting = false;
        ////    //reticle.SetActive(false);
        ////}
        ///
        #endregion


        if (transform.parent.GetComponentInParent<Fov>() != null)
        {
            fov = transform.parent.GetComponentInParent<Fov>();

            if (fov.closetEnemy != null)
            {
                enemy = transform.parent.GetComponentInParent<Fov>().closetEnemy;
                Vector3 direction = enemy.transform.position - transform.position;
                targetRotation = Quaternion.LookRotation(direction);
                lookAt = Quaternion.RotateTowards(transform.rotation, targetRotation, /*Time.fixedDeltaTime **/ rotationSpeed);
                //transform.rotation = lookAt;
                transform.rotation = new Quaternion(0, lookAt.y, 0, lookAt.w);
                targeting = true;
            }
            else
            {
                targeting = false;
                enemy = null;
            }

            if (targeting)
            {
                reticle.SetActive(true);
                reticle.transform.position = enemy.transform.position;
            }
            else
            {
                reticle.SetActive(false);
            }
            if(fov.visibleEnemies.Count ==0 || fov.closetEnemy == null)
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);                                                              // Rotate Gun when not targeting
                targeting = false;
            }
        }

    }

    bool EnemyInFOV(GameObject looker)
    {
        if (enemy != null)
        {
            targetDirection = enemy.transform.position - transform.position;
            float angle = Vector3.Angle(targetDirection, looker.transform.forward);
            if (angle < maxAngle && angle > -maxAngle)
                return true;
        }
        return false;
    }
}


