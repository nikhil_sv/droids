﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitch : MonoBehaviour
{

    public KeyCode switchWeaponButton;
    public GameObject activeWeapon;
    public PlayerSingelton playerSingelton;

    private void Start()
    {
        foreach (Transform weapon in transform)
        {
            if (weapon.GetComponent<WeaponSlot>() != null && weapon.gameObject.activeSelf)
            {
                activeWeapon = weapon.gameObject;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(switchWeaponButton))
        {
            switchWeapons();
        }
    }

    public void switchWeapons()
    {
        foreach (Transform weapon in transform)
        {
            if (weapon.GetComponent<WeaponSlot>() != null)
            {
                weapon.gameObject.SetActive(!weapon.gameObject.activeSelf);
                if (weapon.gameObject.activeSelf)
                    activeWeapon = weapon.gameObject;
                //if(weapon.gameObject !=null)
                //    weapon.gameObject.GetComponent<Weapon>().CheckEquipped();
            }
        }

    }
}
