﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    public float bulletSpeed;
    public float lifeTime;
    public float decay;
    public Vector2 sprayRange;

    private bool isBouncy;
    private bool isPiercing;
    public bool isShotgun;
    public bool isSMG;
    public bool isCrit;


    public GameObject pellet;
    public GameObject muzzleFlash;
    public GameObject hitFlash;

    private WeaponShootStats gunStat;
    private Rigidbody myRb;
    private float allowedHits;

    // Start is called before the first frame update
    void Awake()
    {
        isBouncy = MainSingletons.Instance.perkManager.bounceBullet;
        isPiercing = MainSingletons.Instance.perkManager.piercingBullet;


        if (isPiercing)
        {
            allowedHits = 2;
        }
        if (isPiercing && isBouncy)
        {
            allowedHits = 5;
        }
        myRb = GetComponent<Rigidbody>();

        if(isSMG)
        {
            transform.localEulerAngles += new Vector3(0, Random.Range(sprayRange.x, sprayRange.y), 0);
        }
        
        Instantiate(muzzleFlash, transform.position, transform.rotation);
    }
    private void Start()
    {
        if(isShotgun && pellet != null)
        {
            GameObject pellet1 = Instantiate(pellet, new Vector3(transform.position.x - 2, transform.position.y, transform.position.z), transform.rotation);
            
            GameObject pellet2 = Instantiate(pellet, new Vector3(transform.position.x + 2, transform.position.y, transform.position.z), transform.rotation);
            pellet1.transform.localEulerAngles += new Vector3(0, Random.Range(sprayRange.x, sprayRange.y), 0);
            pellet2.transform.localEulerAngles += new Vector3(0, -Random.Range(sprayRange.x, sprayRange.y), 0);
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        myRb.velocity = transform.forward * bulletSpeed;
        lifeTime -= decay * Time.unscaledDeltaTime;
        if (lifeTime <= 0)
        {
            Destroy(this.gameObject);
        }

    }


    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint hitPoint = collision.contacts[0];
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, hitPoint.normal);
        Vector3 point = hitPoint.point;
        Instantiate(hitFlash, point, rot);


        //if (isBouncy)
        //{

        //}

        if (isPiercing && isBouncy)
        {
            transform.position = point + (transform.forward * 5);
            transform.Rotate(Vector3.up, 90);
            allowedHits -= 1;
        }
        else
        {
            if (isPiercing)
            {

                transform.position = point + (transform.forward * 5);
                allowedHits -= 1;

            }
            else if (isBouncy)
            {
                transform.Rotate(Vector3.up, 90);
                allowedHits -= 1;

            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        

        if (allowedHits <= 0)
        {
            Destroy(this.gameObject);
        }


    }
}
