﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponPickup : MonoBehaviour
{
    public bool isTriggered = false;
    public WeaponSwitch weaponSwitch;
    Transform childGun;
    //public Button equipWeapon;
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<WeaponPickup>()!=null)
        {
            weaponSwitch = other.GetComponentInParent<WeaponSwitch>();
            isTriggered = true;
            ButtonManager.Instance.ExchangeButtons(0, 2);
            if(weaponSwitch!=null)
                weaponSwitch.playerSingelton.LoadWeaponInSight(this);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<WeaponPickup>() != null)
        {
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<WeaponPickup>() != null)
        {
            isTriggered = false;
            ButtonManager.Instance.ExchangeButtons(2, 0);
        }
    }


    private void Start()
    {
       // equipButton = GameObject.Find("EquipWeapon").GetComponent<Joybutton>();
    }
    private void Update()
    {
        //equipWeapon.onClick.AddListener(switchWeapon);
        //if (isTriggered && (Input.GetKeyDown(KeyCode.Z)/*|| equipButton.pressed*/)
        //{
        //    switchWeapon();
        //}
    }

    public void SwitchWeapon()
    {
        Transform parent;
        parent = gameObject.transform.parent;
        gameObject.transform.parent = weaponSwitch.activeWeapon.transform;
        childGun = weaponSwitch.activeWeapon.transform.GetChild(0).transform;
        childGun.parent = parent;
        childGun.GetComponent<WeaponPickup>().weaponSwitch = weaponSwitch;
        //childGun.GetComponent<WeaponPickup>().equipWeapon = equipWeapon;
        // switch position 
        Vector3 temp_position = transform.position;
        transform.position = childGun.position;
        childGun.position = temp_position;

        // enable/disable weapon pickup 

        childGun.GetComponent<WeaponPickup>().enabled = true;
        this.enabled = false;

        childGun.GetComponent<AutoAim>().enabled = false;
        childGun.GetComponent<WeaponShootStats>().enabled = false;
        this.GetComponent<WeaponShootStats>().enabled = true;
        this.GetComponent<AutoAim>().enabled = true;

    }

}
