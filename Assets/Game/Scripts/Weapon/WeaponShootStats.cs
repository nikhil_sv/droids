﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponShootStats : MonoBehaviour
{
    private float timer;
    private float critNum;

    private ShootButton shootbutton;

    public bool isShooting;
    public bool isCrit;

    public float range;
    public float bulletsPerSec;                 // Number of bullets to be fired per second
    public float clipSize;                       // Clip size based on gun type
    public float clipReloadTime;                 // Time required to reload the Clip based on Gun Type
    public bool clipReloading;
    public float clip;                           // Current bullet number in clip being shot
    public float damage;                         // Bullet base damage based on gun type
    public float critDmgMultiplier;              //bullet damage multiplier ie, 2x, 3x, 5x
    public float critDamage;
    public float critChance;                     // Critical hit chance % (/100)
    public float dps;                            // Effective damage per second
    public float fireRate;                       // Effective Rate of Fire

    public Image clipBar;

    private ShootBullet gun;
    private PlayerStats pStat;
    private AutoAim aimStat;
    private PlayerXpManager xpStat;
    // Start is called before the first frame update
    void Start()
    {
        clip = clipSize;

        shootbutton = FindObjectOfType<ShootButton>();
        gun = gameObject.GetComponent<ShootBullet>();
        aimStat = gameObject.GetComponent<AutoAim>();
        pStat = FindObjectOfType<PlayerStats>();
        xpStat = FindObjectOfType<PlayerXpManager>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        critDamage = damage * critDmgMultiplier;

        if(clipBar == null)
        {
            clipBar = GameObject.FindGameObjectWithTag("uiClipbar").GetComponent<Image>();
        }
        clipBar.fillAmount = clip / clipSize;                                                          // Current clip number of bullets available indicator

        aimStat.aimDistance = range;

        timer += Time.unscaledDeltaTime;                                                                  // Timer for maintaining fire rate

        fireRate = 1 / bulletsPerSec;                                                                  //Rate of Fire per second 
        dps = Mathf.Floor((clipSize * damage) / (clipReloadTime + clipSize * fireRate) * 10) / 10;     // DPS = Total Damage of Clip / (Attack Interval + Reload Time).... Value Rounded up to 1 decimal point

        timer += Time.fixedDeltaTime;

        if ((Input.GetKey(KeyCode.T)||shootbutton.shoot) && timer > fireRate && pStat.ammo > 0)
        {
            isShooting = false;
            isCrit = false;
            Shoot();

        }
        if (clip <= 0)                                                                                  // If clip is empty, reload clip
        {
            clipReloading = true;
            /*pStat.*///ammoLeft -= clipSize;                                                                     // Removing clip bullets from Max Ammo Collected
        }
        if (clipReloading)
        {
            reloadClip();
        }
        if (clip >= clipSize)
        {
            clip = clipSize;
            clipReloading = false;
        }
    }

    void Shoot()
    {
        critNum = Random.Range(0, 100);                                                              // Random number between 0 - 100 to decide if Crit Hit

        if (clip > 0 && !clipReloading)
        {
            if (critNum > (100 - critChance))                                                               // If Randum number in Range of Crit%, DO Crit HIT
            {
                //Debug.Log("CRITICAL HIT!" + clip + " , " + critNum);
                gun.CritShoot();                                                                     // Do Critical Shot
                //gun.Shoot();                                                                     // Do Critical Shot
                timer = 0;
                clip -= 1;
                //pStat.ammo -= 1;
                isCrit = true;
            }
            else
            {
                gun.Shoot();                                                                          // Do Normal Shot
                timer = 0;
                //pStat.ammo -= 1;
                clip -= 1;
                isShooting = true;
            }
        }
    }
    void reloadClip()
    {
        clip += (clipSize / clipReloadTime) * Time.unscaledDeltaTime;                                   // Complete loading clip in specified reload time, depending on gun type
    }

    public void UpdateDmg()
    {
        damage += Mathf.RoundToInt((damage * .2f) * xpStat.dmgUpgradeStat);
        pStat.damage = damage;
    }
}
