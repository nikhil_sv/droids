﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerXpManager : MonoBehaviour
{
    private PlayerStats pStats;
    private MenuManager menuStat;
    private WeaponShootStats gunStat;
    public PerkRandomizer perkStat;
    public bool inCombo;
    private float comboTime;


    public float comboCount;
    public float comboMultiplyTime;
    public float maxCombo;
    public float enemyKillCount;
    public float xp;
    public float lvlupCost;
    public float level;
    public float dmgUpgradeStat;

    public Image comboBar;
    public Image xpBar;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI xpText;
    public TextMeshProUGUI comboText;
    public TextMeshProUGUI killText;



    // Start is called before the first frame update
    void Start()
    {
        pStats = FindObjectOfType<PlayerStats>();
        menuStat = FindObjectOfType<MenuManager>();
        gunStat = FindObjectOfType<WeaponShootStats>();
        inCombo = false;
        comboCount = 0;
        maxCombo = 0;
        comboTime = comboMultiplyTime;
        level = 1;
        dmgUpgradeStat = 0;
        levelText.SetText(level.ToString());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // level = pStats.xpLevel;
        comboBar.fillAmount = comboCount / maxCombo;
        xpBar.fillAmount = xp / lvlupCost;
        xpText.SetText(xp.ToString() + " / " + lvlupCost.ToString());
        killText.SetText("Kills: " + enemyKillCount.ToString());

        //if (comboCount > 0)
        //{

        //    inCombo = true;
        //    comboCount -= Time.fixedDeltaTime / comboMultiplyTime;

        //}
        //else if (comboCount <= 0 && inCombo)
        //{

        //    ComboTimeOut();
        //}

        if (xp >= lvlupCost)
        {
            LevelUP();
        }

        //if(maxCombo>0)
        //{
        //    comboText.enabled = true;
        //    comboText.SetText(maxCombo.ToString() + "X");
        //}
        //else
        //{
        //    comboText.enabled = false;
        //}
    }

    public void addCombo()
    {
        enemyKillCount += 1;

        //maxCombo += 1;
        //comboCount = maxCombo;
        // comboMultiplyTime /= 1.5f;
        RewardXP();

    }

    //void ComboTimeOut()
    //{
    //    comboCount = 0;

    //    inCombo = false;
    //    maxCombo = 0;
    //    comboMultiplyTime = comboTime;

    //}

    void RewardXP()
    {
        //Debug.Log("Kill Combo: " + maxCombo);
        xp += 20;
        //maxCombo = 0;
    }

    void LevelUP()
    {
        xp = 0;
        level += 1;
        lvlupCost += 75;
        levelText.SetText(level.ToString());

        //perkStat.ShufflePerks();                                   // Shuffle Perks in Upgrade Menu
        // menuStat.GamePause();                                      // Pause Game
        //menuStat.UpgradeMenu();                                    // Activate upgrade Menu
    }

    public void HPup()
    {
        pStats.maxHealth += Mathf.RoundToInt((pStats.maxHealth) * 0.2f);
        pStats.health += Mathf.RoundToInt((pStats.health) * 0.2f);
    }
    public void DMGup()
    {
        dmgUpgradeStat += 1;

        gunStat.UpdateDmg();
    }
    public void Ammoup()
    {
        pStats.maxAmmo += Mathf.RoundToInt((pStats.maxAmmo) * 0.2f);
        pStats.ammo += Mathf.RoundToInt((pStats.ammo) * 0.2f);
    }
}
