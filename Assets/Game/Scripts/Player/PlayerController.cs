﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    private Joystick joystick;
    private Joybutton dashButton;
    private Joybutton jumpButton;
    [SerializeField]
    private float jumpForce;
    public float speed;

    private bool jump;
    public bool inDash;
    private bool inDashCooldown;
    public float inDashTime;
    public float dashChargeTime;
    public float dashCooldownTime;
    public float dashCool;
    public float dashTrailTimer;
    public int dashMultiplier;
    public bool chargingDash;

    private bool fireDashTrail;
    private bool iceDashTrail;
    private bool acidDashTrail;
    private bool sparkDashTrail;

    private bool fireDashExplosion;
    private bool iceDashExplosion;
    private bool acidDashExplosion;
    private bool sparkDashExplosion;

    private bool fireBallDash;
    private bool iceBallDash;
    private bool acidBallDash;
    private bool sparkBallDash;

    private bool dashMagnet;


    private bool isGrounded;
    private PlayerStats pStats;
    private Vector3 lastPosition;
    public Rigidbody myRb;

    public float brakeForce;
    public Transform droidBody;
    public GameObject dashVfx;
    public GameObject fire;
    public GameObject fireTrail;
    public GameObject fireBall;
    public GameObject ice;
    public GameObject iceTrail;
    public GameObject iceBall;
    public GameObject acid;
    public GameObject acidTrail;
    public GameObject acidBall;
    public GameObject spark;
    public GameObject sparkTrail;
    public GameObject sparkBall;
    public GameObject magnet;

    private PerkManager perkManager;

    //public GameObject dashBlast;
    public Image dashChargeBar;
    public SpriteRenderer playerLoc;


    [SerializeField]
    public float charge;



    private void Start()
    {
        joystick = FindObjectOfType<Joystick>();
        dashButton = FindObjectOfType<Joybutton>();
        //jumpButton = GameObject.Find("Jump").GetComponent<Joybutton>();
        perkManager = MainSingletons.Instance.perkManager;
        
        myRb = GetComponent<Rigidbody>();
        pStats = FindObjectOfType<PlayerStats>();
        charge = 0;
        speed = pStats.speed;
        inDash = false;

        fireDashTrail = perkManager.dashFireTrail;
        iceDashTrail = perkManager.dashIceTrail;
        acidDashTrail = perkManager.dashAcidTrail;
        sparkDashTrail = perkManager.dashSparkTrail;

        fireDashExplosion = perkManager.dashFireExplosion;
        iceDashExplosion = perkManager.dashIceExplosion;
        acidDashExplosion = perkManager.dashAcidExplosion;
        sparkDashExplosion = perkManager.dashSparkExplosion;

        fireBallDash = perkManager.dashFireBall;
        iceBallDash = perkManager.dashIceBall;
        acidBallDash = perkManager.dashAcidBall;
        sparkBallDash = perkManager.dashSparkBall;

        dashMagnet = perkManager.dashMagnet;

    }

    private void FixedUpdate()
    {

        if (dashCool < dashCooldownTime)
        {
            dashCool += Time.fixedDeltaTime;
            inDash = true;

        }
        if (dashCool > inDashTime)                            // To turn off inDash Powers
        {
            //dashCool = 0;
            inDash = false;
        }
        if (dashCool >= dashCooldownTime)
        {
            playerLoc.color = Color.cyan;
        }
        else
        {
            playerLoc.color = Color.red;
        }
        //Rigidbody rigidbody = GetComponent<Rigidbody>();
        //rigidbody.velocity = new Vector3(joystick.Horizontal * speed, rigidbody.velocity.y, joystick.Vertical * speed);
        Vector3 direction = new Vector3(joystick.Horizontal, 0, joystick.Vertical).normalized;                                 // Normalised direction vector
        //Vector3 direction = Vector3.forward * joystick.Vertical + Vector3.right * joystick.Horizontal;                       // Original Direction Calculation
        //myRb.AddForce(direction.normalized * speed, ForceMode.VelocityChange);                                               // Smooth acceleration movement 
        //rigidbody.velocity = (direction.normalized * speed);                                                                 // Snappy movement by changing velocity


        myRb.velocity = Vector3.Lerp(myRb.velocity, (direction.normalized * speed), 0.1f);                                 // Snappy movement by changing velocity


        //dashChargeBar.fillAmount = charge / dashChargeTime;
        //Debug.Log(dashCool);


        //myRb.MovePosition(transform.position + direction * speed * Time.fixedDeltaTime);

        if ((dashButton.pressed || Input.GetKey(KeyCode.E)) && dashCool >= dashCooldownTime)
        {
            chargingDash = true;
            ChargeDash();
        }
        else if (charge > 0 && !chargingDash)
        {

            Dash();
        }
        else
        {
            chargingDash = false;
        }



        if (isGrounded)
        {
            lastPosition = transform.position;
        }
        if (joystick.Vertical == 0 && joystick.Horizontal == 0)
        {
            AddBrake();
        }

        if (inDash)
        {

            dashTrailTimer -= Time.fixedDeltaTime;                                                 // Dash Trail
            if (dashTrailTimer <= 0)
            {
                if (fireDashTrail)                                                                 // Fire Dash Trail
                {
                    Instantiate(fireTrail, transform.position, Quaternion.identity);
                }
                if (iceDashTrail)
                {
                    Instantiate(iceTrail, transform.position, Quaternion.identity);                // Ice Dash Trail
                }
                if (acidDashTrail)
                {
                    Instantiate(acidTrail, transform.position, Quaternion.identity);                // Ice Dash Trail
                }
                if (sparkDashTrail)
                {
                    Instantiate(sparkTrail, transform.position, Quaternion.identity);                // Ice Dash Trail
                }
                dashTrailTimer = 0.08f;
            }
        }


    }


    public void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }

    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }

    void AddBrake()
    {

        myRb.AddForce(-myRb.velocity * brakeForce);
    }
    public void Dash()
    {
        dashTrailTimer = 0.08f;
        TimeManager.Instance.ResetTime();                                    // Reset Time before dash

        if (fireDashExplosion)                                               // List of Dash Perks
        {
            FireBurst();
        }
        if (fireBallDash)
        {
            FireBall();
        }
        if (iceDashExplosion)
        {
            IceBurst();
        }
        if (iceBallDash)
        {
            IceBall();
        }
        if (acidDashExplosion)
        {
            AcidBurst();
        }
        if (acidBallDash)
        {
            AcidBall();
        }
        if (sparkDashExplosion)
        {
            SparkBurst();
        }
        if (sparkBallDash)
        {
            SparkBall();
        }


        if (dashMagnet)
        {
            Instantiate(magnet, transform.position, Quaternion.identity);
        }


        //if (pStats.charge >= pStats.maxCharge)
        {
            float dashBoost = Mathf.Round((charge / dashChargeTime) * dashMultiplier) + 1;                         // Increase Dash power based on Dash Hold Time
                                                                                                                   //Debug.Log("Boost Multiplier: " + dashBoost);
            dashCool = 0;
            charge = 0;
            pStats.charge = 0;

            //Instantiate(dashBlast, droidBody.transform.position, Quaternion.identity);
            Instantiate(dashVfx, droidBody.transform.position, droidBody.transform.rotation);

            if (fireBallDash || iceBallDash || acidBallDash || sparkBallDash)
            {
                myRb.AddForce(droidBody.transform.forward * jumpForce/ 2, ForceMode.Impulse);
            }
            else
            {
                myRb.AddForce(droidBody.transform.forward * jumpForce, ForceMode.Impulse);
            }




        }
    }
    public void ChargeDash()
    {

        charge += Time.fixedDeltaTime;
        myRb.velocity /= 1.1f;
        //Debug.Log("Charge: " + charge);
        TimeManager.Instance.DashSlowTime();                           // Slow Time while charging dash
        if (charge >= dashChargeTime)
        {
            
            Dash();
        }

    }

    public void ResetCooldown()
    {
        dashCool = dashCooldownTime;
    }



    //Perk Functions: 

    public void FireBurst()
    {
        int spawnNum = Mathf.RoundToInt(Random.Range(4, 10));
        for (int i = spawnNum; i > 0; i--)
        {
            Instantiate(fire, transform.position, Quaternion.identity);
        }
    }

    public void FireBall()
    {
        GameObject ball = Instantiate(fireBall, droidBody.transform.position, droidBody.transform.rotation);

        
    }

    public void IceBurst()
    {
        int spawnNum = Mathf.RoundToInt(Random.Range(4, 10));
        for (int i = spawnNum; i > 0; i--)
        {
            Instantiate(ice, transform.position, Quaternion.identity);

        }
    }

    public void IceBall()
    {
        GameObject ball = Instantiate(iceBall, droidBody.transform.position, droidBody.transform.rotation);

        
    }

    public void AcidBurst()
    {
        int spawnNum = Mathf.RoundToInt(Random.Range(4, 10));
        for (int i = spawnNum; i > 0; i--)
        {
            Instantiate(acid, transform.position, Quaternion.identity);

        }
    }

    public void AcidBall()
    {
        GameObject ball = Instantiate(acidBall, droidBody.transform.position, droidBody.transform.rotation);

        
    }

    public void SparkBurst()
    {
        int spawnNum = Mathf.RoundToInt(Random.Range(4, 10));
        for (int i = spawnNum; i > 0; i--)
        {
            Instantiate(spark, transform.position, Quaternion.identity);

        }
    }

    public void SparkBall()
    {
        GameObject ball = Instantiate(sparkBall, droidBody.transform.position, droidBody.transform.rotation);

        
    }


}
