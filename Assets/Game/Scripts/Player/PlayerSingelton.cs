﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSingelton : MonoBehaviour
{
    // remove this script and add the function to another script 
    public WeaponPickup weaponInSight;

    public void LoadWeaponInSight(WeaponPickup weapon)
    {
        weaponInSight = weapon;
    }
}
