using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using BayatGames.SaveGameFree.Examples;
using System.Runtime.InteropServices;

public class PlayerStats : MonoBehaviour
{
    public Image HealthBar;
    public Image AmmoBar;
    public Image XPBar;
    public TextMeshProUGUI healthNum;
    public Image HealthBarBG;
    public TextMeshProUGUI ammoNum;
    public TextMeshProUGUI skillPointsNum;
    public Text coinNum;
    public Image ChargeBar;

    public float health;
    public float maxHealth;
    public float resistPercent;                 // Percentage of Damage take reduced
    public float HPRegen;                       // Percentage of MaxHp to regenerate
    public float ammo;
    public float maxAmmo;
    public float ammoFillrate;
    public float maxCharge;
    public float charge;
    public float chargeFillrate;
    public bool reloading;
    public bool loadedStats;
    public float dashDamage;
    public float skillPoints;
    public float levelCost;
    public int skillLevel;
    public float damage;
    public string weaponName;
    public int coins;
    public float speed;
    public string perkString;

    public GameObject HitVFX;
    public GameObject LvlUpVFX;
    public WeaponSlot activeWepaonSlot;
    public WeaponShootStats activeWeapon;
    public PlayerController pControl;
    public CameraShakeControl camShake;
    public PlayerData playerData;
    public LevelManager levelManager;
    private float dmgFlashTime;
    public GameObject[] weaponsList;

    private DamageDisplay dmgDisplay;

    // Start is called before the first frame update
    void Start()
    {
        pControl = gameObject.GetComponent<PlayerController>();
        dmgDisplay = gameObject.GetComponent<DamageDisplay>();
        playerData = GameObject.Find("GameStats").GetComponent<PlayerData>();
        //loadedStats = false;
        weaponName = PlayerPrefs.GetString("EquippedWeapon");
        weaponsList = Resources.LoadAll<GameObject>("Weapons");
        for(int i = 0; i < weaponsList.Length; i++)
        {
            if(weaponsList[i].name == weaponName)
            {
                activeWeapon.gameObject.SetActive(false);
                GameObject tempWeapon= Instantiate(weaponsList[i], activeWeapon.transform.parent) as GameObject;
            }
        }
        //camShake = FindObjectOfType<CameraShakeControl>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (skillPoints > levelCost)
        {
            skillPoints = 0;
            skillLevel += 1;
            levelCost += Mathf.RoundToInt(levelCost / 2);
            Instantiate(LvlUpVFX, transform.position, transform.rotation);
        }

        activeWeapon = activeWepaonSlot.GetComponentInChildren<WeaponShootStats>();

        HealthBar.fillAmount = health / maxHealth;
        AmmoBar.fillAmount = ammo / maxAmmo;
        XPBar.fillAmount = skillPoints / levelCost;
        healthNum.SetText(Mathf.RoundToInt(health).ToString() + " / " + Mathf.RoundToInt(maxHealth).ToString());
        ammoNum.SetText(ammo.ToString() + " / " + maxAmmo.ToString());
        skillPointsNum.SetText(skillLevel.ToString());
        //ChargeBar.fillAmount = charge / maxCharge;
        //ChargeBar.fillAmount = (pControl.dashCool / pControl.dashCooldownTime);
        if (ammo <= 0)
        {
            Reload();                // Reload Gun when ammo is empty
        }
        if (charge >= 0)
        {
            charge += chargeFillrate * Time.deltaTime;
        }
        if (reloading)
        {
            ammo += ammoFillrate * Time.deltaTime;
        }
        //if (ammo >= maxAmmo)                             // Max Ammo was getting loaded to 0, so ammo was also becoming 0... LOL
        //{
        //    ammo = maxAmmo;
        //    reloading = false;
        //}
        //if (ammo < 0)
        //{
        //    ammo = 0;
        //}
        if (charge >= maxCharge)
        {
            charge = maxCharge;
        }


        if (health <= maxHealth / 3)
        {
            HealthBar.color = Color.red;
        }
        else
        {
            if (dmgFlashTime > 0)
            {
                dmgFlashTime -= Time.fixedDeltaTime;
            }
            else if (dmgFlashTime <= 0)
            {
                HealthBar.color = Color.green;
            }
        }
        if (health > maxHealth)
        {
            health = maxHealth;
        }

        if(health < maxHealth)
        {
            health += (HPRegen * (maxHealth/100)) * Time.fixedDeltaTime/60;                        // Regenerate percentage of MaxHp per minute
        }

        SetPlayerData();
    }

    void SetPlayerData()
    {
        playerData.SetAmmo(ammo);
        playerData.SetHealth(health);
        playerData.SetMaxAmmo(maxAmmo);
        playerData.SetMaxHealth(maxHealth);
        playerData.SetSkillLevel(skillLevel);
        playerData.SetSkillPoints(skillPoints);
        playerData.SetLevelCost(levelCost);
        playerData.SetCoins(coins);
        playerData.SetPerkString(perkString);
    }
    void Reload()
    {
        reloading = true;
    }

    public void TakeDamage(float damage)
    {
        if (!pControl.inDash)
        {
            TimeManager.Instance.KillTime();
            health -= damage * (1 - resistPercent/100);
            dmgDisplay.DisplayDamage(damage * (1 - resistPercent / 100));
            DamageFlash();

            camShake.bigShake();
            pControl.myRb.velocity = (-pControl.myRb.velocity * 1.5f);
            Instantiate(HitVFX, transform.position, Quaternion.identity);
        }
    }
    public void DamageFlash()                              // Flash Red When Hurt
    {
        HealthBar.color = Color.red;
        dmgFlashTime = 0.2f;                               // Flashing Time
    }

    public void AddSkillPoints(float value)
    {
        skillPoints += value;
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Hazzard") || collision.gameObject.CompareTag("pDamage") || collision.gameObject.CompareTag("Spike"))
        {
            TakeDamage(2);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Spike") || collision.gameObject.CompareTag("pDamage"))
        {
            TakeDamage(2);
        }
    }

    public void IncreaseHealth(int value)
    {
        if (skillLevel > 0)
        {
            maxHealth += value;
            skillLevel--;
        }
        else
        {
            Debug.LogError("Do not have enough Skill Points");
        }
    }
    public void DecreaseHealth(int value)
    {
        if (maxHealth > playerData.tempMaxHealth)
        {
            maxHealth -= value;
            skillLevel++;
        }
        else
        {
            Debug.LogError("Max Health can't be reduced ");
        }
    }

    public void IncreaseAmmo(int value)
    {
        if (skillLevel > 0)
        {
            maxAmmo += value;
            skillLevel--;
        }
        else
        {
            Debug.LogError("Do not have enough Skill Points");
        }
    }
    public void DecreaseAmmo(int value)
    {
        if (maxAmmo > playerData.tempMaxAmmo )
        {
            maxAmmo -= value;
            skillLevel++;
        }
        else
        {
            Debug.LogError("Max Ammo can't be reduced");
        }
    }

    public void IncreaseSpeed(int value)
    {
        if (skillLevel > 0)
        {
            pControl.speed += value;
            skillLevel--;
        }
        else
        {
            Debug.LogError("Do not have enough Skill Points");
        }
    }

    public void DecreaseSpeed(int value)
    {
        if (pControl.speed > playerData.tempSpeed)
        {
            pControl.speed -= value;
            skillLevel++;
        }
        else
        {
            Debug.LogError("Speed can't be reduced");
        }
    }

}
