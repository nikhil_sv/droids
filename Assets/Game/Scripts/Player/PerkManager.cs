﻿using System.Runtime.Serialization.Formatters;
using UnityEngine;

public class PerkManager : MonoBehaviour
{
    public bool doubleBullet ;
    public bool diagonalBullet ;
    public bool bounceBullet ;
    public bool piercingBullet ;
    public bool fireBullet ;
    public bool iceBullet ;
    public bool acidBullet ;
    public bool sparkBullet ;
    public bool dashFireTrail ;
    public bool dashIceTrail ;
    public bool dashAcidTrail;
    public bool dashSparkTrail;
    public bool dashFireExplosion ;
    public bool dashIceExplosion ;
    public bool dashAcidExplosion;
    public bool dashSparkExplosion;
    public bool dashMagnet ;
    public bool dashFireBall ;
    public bool dashIceBall ;
    public bool dashAcidBall;
    public bool dashSparkBall;
    public bool fireOrbiter ;
    public bool iceOrbiter ;
    public bool bladeOrbiter ;

    public GameObject fireOrbiterGO;
    public GameObject iceOrbiterGO;
    public GameObject bladeOrbiterGO;
    public GameObject fireBulletGO;
    public GameObject iceBulletGO;
    public GameObject acidBulletGO;
    public GameObject sparkBulletGO;

    public bool[] perksBools;
    public string[] perksBoolsNames;
    public string perkString = "";
    public string tempString = "";
    public bool decode = false;
    private void Update()
    {
        fireOrbiterGO.SetActive(fireOrbiter);
        iceOrbiterGO.SetActive(iceOrbiter);
        bladeOrbiterGO.SetActive(bladeOrbiter);


        EncodePerksBool();
        //if(decode)
        //{
        //    print("Entered");
        if (tempString != "")
        {
            DecodePerksBool(tempString);
            tempString = "";
        }
        //    decode = false;
        //}

    }

    public void StartDecode(string value)
    {
        tempString = value;
        decode = true;
    }
    public void SetPerksBool()
    {
        perksBools = new bool[] { doubleBullet, diagonalBullet, bounceBullet, piercingBullet, fireBullet, iceBullet,acidBullet,sparkBullet,
                                  dashFireTrail, dashIceTrail,dashAcidTrail,dashSparkTrail,dashFireExplosion,dashIceExplosion,dashAcidExplosion,
                                  dashSparkExplosion, dashMagnet,dashFireBall,dashIceBall,dashAcidBall,dashSparkBall, fireOrbiter,iceOrbiter,bladeOrbiter};
       
        perksBoolsNames = new string[] { "Double Bullet", "Diagonal Bullet", "Bounce Bullet", "Piercing Bullet", "Fire Bullet", "Ice Bullet"," Acid Bullet","Spark Bullet",
                                  "Dash Fire Trail", "Dash Ice Trail","Dash Acid Trail","Dash Spark Trail","Dash Fire Explosion","Dash Ice Explosion","Dash Acid Explosion",
                                  "Dash Spark Explosion", "Dash Magnet","Dash Fire Ball","Dash Ice Ball","Dash Acid Ball","Dash Spark Ball", "Fire Orbiter","Ice Orbiter","Blade Orbiter"};
    }
    public void SetPerksVariables()
    {
        doubleBullet = perksBools[0];
        diagonalBullet = perksBools[1];
        bounceBullet = perksBools[2];
        piercingBullet = perksBools[3];
        fireBullet = perksBools[4];
        iceBullet = perksBools[5];
        acidBullet = perksBools[6];
        sparkBullet = perksBools[7];
        dashFireTrail = perksBools[8];
        dashIceTrail = perksBools[9];
        dashAcidTrail = perksBools[10];
        dashSparkTrail = perksBools[11];
        dashFireExplosion = perksBools[12];
        dashIceExplosion = perksBools[13];
        dashAcidExplosion = perksBools[14];
        dashSparkExplosion = perksBools[15];
        dashMagnet = perksBools[16];
        dashFireBall = perksBools[17];
        dashIceBall = perksBools[18];
        dashAcidBall = perksBools[19];
        dashSparkBall = perksBools[20];
        fireOrbiter = perksBools[21];
        iceOrbiter = perksBools[22];
        bladeOrbiter = perksBools[23];
        EncodePerksBool();
    }
    public void EncodePerksBool()
    {
        perkString = "";
        SetPerksBool();
        for (int i = 0; i<perksBools.Length;i++)
        {
            if(perksBools[i])
            {
                perkString = perkString + "a";
            }
            else
            {
                perkString = perkString + "b";
            }
        }
        MainSingletons.Instance.playerStats.perkString = perkString;
    }

    public void DecodePerksBool(string value)
    {
        char[] characters = new char[perksBools.Length];
        if (value != "" || value != null)
        {
            for (int i = 0; i < value.Length; i++)
            {
                characters[i] = (value[i]);
                if (characters[i] == 'a')
                {
                    switch (i)
                    {
                        case 0:
                            doubleBullet = true;
                            break;
                        case 1:
                            diagonalBullet = true;
                            break;
                        case 2:
                            bounceBullet = true;
                            break;
                        case 3:
                            piercingBullet = true;
                            break;
                        case 4:
                            fireBullet = true;
                            break;
                        case 5:
                            iceBullet = true;
                            break;
                        case 6:
                            iceBullet = true;
                            break;
                        case 7:
                            iceBullet = true;
                            break;
                        case 8:
                            dashFireTrail = true;
                            break;
                        case 9:
                            dashIceTrail = true;
                            break;
                        case 10:
                            dashAcidTrail = true;
                            break;
                        case 11:
                            dashSparkTrail = true;
                            break;
                        case 12:
                            dashFireExplosion = true;
                            break;
                        case 13:
                            dashIceExplosion = true;
                            break;
                        case 14:
                            dashAcidExplosion = true;
                            break;
                        case 15:
                            dashSparkExplosion = true;
                            break;
                        case 16:
                            dashMagnet = true;
                            break;
                        case 17:
                            dashFireBall = true;
                            break;
                        case 18:
                            dashIceBall = true;
                            break;
                        case 19:
                            dashAcidBall = true;
                            break;
                        case 20:
                            dashSparkBall = true;
                            break;
                        case 21:
                            fireOrbiter = true;
                            break;
                        case 22:
                            iceOrbiter = true;
                            break;
                        case 23:
                            bladeOrbiter = true;
                            break;
                        default:
                            print("Default Value");
                            break;
                    }
                }
            }
        }
    }
}
