﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fov : MonoBehaviour
{
    public float viewRadius;
    [Range(0,360)]
    public float viewAngle;

    public LayerMask enemyMask;
    public LayerMask obstacleMask;
    public List<Transform> visibleEnemies = new List<Transform>();
    public GameObject closetEnemy;

    private void Update()
    {
        FindVisibleEnemies();
    }
    void FindVisibleEnemies()
    {
        visibleEnemies.Clear();
        Collider[] enemiesInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, enemyMask);

        for(int i =0; i < enemiesInViewRadius.Length;i++)
        {
            Transform enemy = enemiesInViewRadius[i].transform;
            Vector3 dirToEnemy = (enemy.position - transform.position).normalized;
            if(Vector3.Angle(transform.forward,dirToEnemy) < viewAngle/2)
            {
                float distToEnemy = Vector3.Distance(transform.position, enemy.position);
                if(!Physics.Raycast(transform.position,dirToEnemy,distToEnemy,obstacleMask))
                {
                    visibleEnemies.Add(enemy);
                }
            }
        }
        ClosestVisibleEnemy();
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if(!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    public void ClosestVisibleEnemy()
    {
        if (visibleEnemies.Count > 0)
        {
            float tempDist = Vector3.Distance(transform.position, visibleEnemies[0].position);

            foreach (Transform transformGO in visibleEnemies)
            {
                float distance = Vector3.Distance(transformGO.position, transform.position);
                if (distance <= tempDist)
                {
                    tempDist = distance;
                    closetEnemy = transformGO.gameObject;
                    GetComponent<EnemyManager>().closetEnemy = closetEnemy;
                }
            }
        }
    }
}
