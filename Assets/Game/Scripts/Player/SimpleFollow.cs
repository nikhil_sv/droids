﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFollow : MonoBehaviour
{
    public GameObject target;

    public float xOff;
    public float yOff;
    public float zOff;
    public float followSpeed;
    public float rotateSpeed;
    public FixedJoystick varJoy;

    public bool smoothfollow;

    // Start is called before the first frame update
    void Start()
    {

    }
    private void Update()
    {
        //transform.position = new Vector3(target.transform.position.x + xOff, target.transform.position.y + yOff, target.transform.position.z + zOff);

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 playerPos = new Vector3(target.transform.position.x + xOff, target.transform.position.y + yOff, target.transform.position.z + zOff);

        if (smoothfollow)
        {
            transform.position = Vector3.Lerp(transform.position, playerPos, Time.deltaTime * followSpeed);
        }
        else
        {
            transform.position = playerPos;
        }
        if (varJoy != null)
        {
            if (varJoy.Horizontal != 0 && varJoy.Vertical != 0)
            //RotateAim();
            //if (!aimStats.targeting)
            {
                RotateAim();
            }
        }

    }

    void RotateAim()
    {
        Vector3 joyDir = Vector3.right * varJoy.Horizontal + Vector3.forward * varJoy.Vertical;
        //Vector2 aimDir = joyDir - new Vector3(transform.position.x, transform.position.z);
        //transform.Rotate(joyDir);
        Quaternion angle = Quaternion.LookRotation(joyDir, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, angle, Time.fixedDeltaTime * rotateSpeed);
    }
}
