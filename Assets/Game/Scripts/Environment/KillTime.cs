﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillTime : MonoBehaviour
{
    private float timer;
    public float killtime;
    // Start is called before the first frame update
    void Start()
    {
        timer = killtime;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if(timer<=0)
        {
            Destroy(this.gameObject);
        }
    }
}
