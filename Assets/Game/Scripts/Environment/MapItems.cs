﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct MapItems 
{
    public Color _color;
    public GameObject gameObject;
}
