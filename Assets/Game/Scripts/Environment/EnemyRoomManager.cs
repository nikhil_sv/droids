﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRoomManager : MonoBehaviour
{
    public EnemySpawner enemySpawner;
    public int enemiesKilled = 0;

    private void Start()
    {
        enemySpawner = GetComponentInChildren<EnemySpawner>();
        if(enemySpawner == null)
        {
            Debug.LogError("Attach EnemySpawner to " + transform.name);
        }
    }
}
