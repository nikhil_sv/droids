﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinManager : MonoBehaviour
{
    public int coinsCollected;
    public Text coinCollect;
    public PlayerStats playerStats;
    // Start is called before the first frame update
    private void Start()
    {

    }
    

    // Update is called once per frame
    void Update()
    {
        //if(coinCollect == null)
        //{
        //   coinCollect = GameObject.Find("CoinCount").GetComponent<Text>();
        //}
        coinCollect.text = "Coins:" + coinsCollected;
        playerStats.coins = coinsCollected;
    }
    public void AddCoin()
    {
        coinsCollected += 1;
    }
}
