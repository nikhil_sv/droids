﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelGenerator : MonoBehaviour
{
    public GameObject templateParent;
    public Sprite mapSprite;
    public int scaleFactor;
    public Vector3 worldRotate;
    public NavMeshSurface surface;
    public Dictionary<Color, GameObject> legend = new Dictionary<Color, GameObject>();
    public List<MapItems> mapItems = new List<MapItems>();
    public List<GameObject> mapFloors = new List<GameObject>();
    Sprite[] maps;

    private void Start()
    {
        LoadMap();
        GenerateDictionary();
        StartNewLevel();
    }

    public void StartNewLevel()
    {
        GenerateMap();
        transform.localEulerAngles = worldRotate;
        surface.BuildNavMesh();
    }
    void LoadMap()
    {
        maps = Resources.LoadAll<Sprite>("Maps");
    }
    void GenerateDictionary()
    {
        for(int i =0;i<mapItems.Count;i++)
        {
            legend.Add(mapItems[i]._color, mapItems[i].gameObject);
        }
    }

    void GenerateMap()
    {
        mapSprite = maps[Random.Range(0, maps.Length)];
        int width = mapSprite.texture.width;
        int height = mapSprite.texture.height;

        for(int x= 0; x<width;x++)
        {
            for(int z=0;z<height;z++)
            {
                Color pixelColor = mapSprite.texture.GetPixel(x, z);
                GameObject tempGO = FindInDictionary(pixelColor);
                //if (pixelColor.a != 0)
                //{
                //    Debug.Log(pixelColor );
                //}
                if (tempGO !=null)
                {
                   // Debug.Log("PRINTED : " + pixelColor + " " + tempGO.name);
                    Vector3 position = new Vector3(x * scaleFactor, 0, z * scaleFactor);
                    GameObject placedTempGo = Instantiate(tempGO, position, Quaternion.identity, templateParent.transform);
                    mapFloors.Add(placedTempGo);
                }
            }
        }
    }

    private GameObject FindInDictionary(Color _color)
    {
        if(legend.ContainsKey(_color))
        {
            return legend[_color];
        }
        return null;
    }

    public void DeleteLevelGenerator()
    {
        mapSprite = null;
        int childs = templateParent.transform.childCount;
        for (int i = 0; i<childs; i++)
        {
            Destroy(templateParent.transform.GetChild(i).gameObject);
        }
        GetComponentInChildren<RoomPlacer>().DeleteRooms();
    }
}
