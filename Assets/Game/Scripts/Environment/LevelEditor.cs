﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelEditor : MonoBehaviour
{
    public Texture2D map;
    public float scaleFactor;
    public ColorToPRefab[] colorMappings;
    public List<GameObject> mapFloors = new List<GameObject>();
    public NavMeshSurface surface;
    private void Start()
    {
        GenerateLevel();
        surface.BuildNavMesh();
        transform.localEulerAngles = new Vector3(0, -45, 0);
    }

    void GenerateLevel()
    {
        for(int x=0; x<map.height;x++)
        {
            for(int y=0; y<map.width;y++)
            {
                GenerateTile(x,y);
            }
        }
    }

    void GenerateTile(int x, int y)
    {
        Color pixelColor = map.GetPixel(x, y);
        if(pixelColor.a ==0)
        {
            return; // transparent pixels ignore
        }
        GameObject mapGO;
        foreach(ColorToPRefab colorMapping in colorMappings)
        {
            if(colorMapping.color.Equals(pixelColor))
            {
                Vector3 position = new Vector3(x * scaleFactor, 0, y * scaleFactor);
                mapGO = Instantiate(colorMapping.prefab, position, Quaternion.identity,transform);
                mapFloors.Add(mapGO);
            }
        }
    }
}
