﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RoomPlacer : MonoBehaviour
{
    //LevelEditor levelEditor;
    public GameObject player;
    LevelGenerator levelGenerator;
    public GameObject[] pathRooms;
    public GameObject[] enemyRooms;
    public GameObject[] startRooms;
    public GameObject[] endRooms;
    public GameObject[] lootRooms;
    // add different kinds of rooms
    private void Start()
    {
        //levelEditor = GetComponentInParent<LevelEditor>();
        levelGenerator = GetComponentInParent<LevelGenerator>();
        LoadRooms();
        PlaceRooms();
        //surface = GetComponent<NavMeshSurface>();

    }
    public void PlaceRooms()
    {
        PlaceStartRoom();
        PlacePathRoom();
        PlaceLootRoom();
        PlaceEnemyRoom();
        PlaceEndRoom();
    }
    void LoadRooms()
    {
        pathRooms = Resources.LoadAll<GameObject>("PathRooms");
        enemyRooms = Resources.LoadAll<GameObject>("EnemyRooms");
        startRooms = Resources.LoadAll<GameObject>("StartRooms");
        endRooms = Resources.LoadAll<GameObject>("EndRooms");
        lootRooms = Resources.LoadAll<GameObject>("LootRooms");

        // load different kinds of rooms
    }

    public static void RemoveAt<T>(ref T[] arr, int index) // used to not repeat the rooms placed 
    {
        for (int a = index; a < arr.Length - 1; a++)
        {
            // moving elements downwards, to fill the gap at [index]
            arr[a] = arr[a + 1];
        }
        // finally, let's decrement Array's size by one
        Array.Resize(ref arr, arr.Length - 1);
    }
    void PlaceStartRoom()
    {
        GameObject startRoom;
        int randomNumber = UnityEngine.Random.Range(0, startRooms.Length);
        if (startRooms[randomNumber] != null)
        {
            startRoom = startRooms[randomNumber];
            GameObject startRoomPlane;
            for (int i = 0; i < levelGenerator.mapFloors.Count; i++)
            {
                startRoomPlane = levelGenerator.mapFloors[i];
                if (startRoomPlane.GetComponent<StartRoomPlane>() != null)
                {
                    Instantiate(startRoom, startRoomPlane.transform.position, startRoomPlane.transform.rotation, transform);
                    //RemoveAt(ref startRooms, randomNumber);
                    levelGenerator.mapFloors.Remove(startRoomPlane);
                    player.transform.position = startRoomPlane.GetComponent<StartRoomPlane>().playerSpawn.transform.position + new Vector3(0, 2, 0);
                    break;
                }
            }

        }
    }
    void PlaceEndRoom()
    {
        GameObject endRoom;
        int randomNumber = UnityEngine.Random.Range(0, endRooms.Length);
        if (endRooms[randomNumber] != null)
        {
            endRoom = endRooms[randomNumber];
            GameObject endRoomPlane;
            for (int i = 0; i < levelGenerator.mapFloors.Count; i++)
            {
                endRoomPlane = levelGenerator.mapFloors[i];
                if (endRoomPlane.GetComponent<EndRoomPlane>() != null)
                {
                    Instantiate(endRoom, endRoomPlane.transform.position, endRoomPlane.transform.rotation, transform);
                    //RemoveAt(ref startRooms, randomNumber);
                    levelGenerator.mapFloors.Remove(endRoomPlane);
                    break;
                }
            }
        }
        FinishedRoomPlacement();
    }
    void PlaceLootRoom()
    {
        GameObject lootRoom;
        int randomNumber = UnityEngine.Random.Range(0, lootRooms.Length);
        if (lootRooms[randomNumber] != null)
        {
            lootRoom = lootRooms[randomNumber];
            GameObject lootRoomPlane;
            for (int i = 0; i < levelGenerator.mapFloors.Count; i++)
            {
                lootRoomPlane = levelGenerator.mapFloors[i];
                if (lootRoomPlane.GetComponent<LootRoomPlane>() != null)
                {
                    Instantiate(lootRoom, lootRoomPlane.transform.position, lootRoomPlane.transform.rotation, transform);
                    //RemoveAt(ref startRooms, randomNumber);
                    levelGenerator.mapFloors.Remove(lootRoomPlane);
                    break;
                }
            }
        }
    }
    void PlacePathRoom()
    {
        GameObject pathRoom;
        // place random range line here if you need to apply same path for every tile for every time you play the game
        int randomNumber = UnityEngine.Random.Range(0, pathRooms.Length);
        // foreach (GameObject roomPlane in levelGenerator.mapFloors)
        for (int i = 0; i < levelGenerator.mapFloors.Count - 1; i++)
        {
            if (pathRooms[randomNumber] != null)
            {
                // place random range line here if you need different path for different tiles 
                pathRoom = pathRooms[UnityEngine.Random.Range(0, pathRooms.Length)];
                GameObject pathPlane = levelGenerator.mapFloors[i];
                if (pathPlane.GetComponent<PathPlane>() != null)
                {
                    // Debug.Log(pathPlane.transform.position);
                    Instantiate(pathRoom, pathPlane.transform.position, pathPlane.transform.rotation, transform);
                    //RemoveAt(ref pathRooms, randomNumber);
                }
            }
        }
    }

    void PlaceEnemyRoom()
    {
        GameObject enemyRoom;
        // place random range line here if you need to apply same path for every tile for every time you play the game
        //foreach (GameObject roomPlane in levelEditor.mapFloors)

        for (int i = 0; i < levelGenerator.mapFloors.Count - 1; i++)
        {
            int randomNumber = UnityEngine.Random.Range(0, enemyRooms.Length);
            if (enemyRooms[randomNumber] != null)
            {
                enemyRoom = enemyRooms[randomNumber];
                GameObject roomPlane = levelGenerator.mapFloors[i];
                if (roomPlane.GetComponent<RoomPlane>() != null)
                {
                    GameObject instantiatedRoom = Instantiate(enemyRoom, roomPlane.transform.position, roomPlane.transform.rotation, transform);
                    //RemoveAt(ref enemyRooms, randomNumber);
                    //if(instantiatedRoom.GetComponentInChildren<EnemySpawner>()!=null)
                    //{
                    //    instantiatedRoom.GetComponentInChildren<EnemySpawner>().SpawnEnemy();
                    //}
                    levelGenerator.mapFloors.Remove(roomPlane);
                }
            }
        }
    }

    void FinishedRoomPlacement()
    {
        MainSingletons.Instance.enemyManager.PopulateEnemyList();
        GunVariablesManager.Instance.GatherGuns();
    }

    public void DeleteRooms()
    {
        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
}
