﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public bool overlapPoint = false;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer.Equals(10))
        {
            overlapPoint = true;
        }
    }
}
