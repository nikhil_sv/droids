﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShakeControl : MonoBehaviour
{
    public float shakeDuration;
    public float shakeAmplitude;
    public float shakeFrequency;

    private float shakeElapsedTime = 0f;

    public CinemachineVirtualCamera myCam;
    private CinemachineBasicMultiChannelPerlin camNoise;
    public ShootButton gun;
    // Start is called before the first frame update
    void Start()
    {
        if (myCam != null)
            camNoise = myCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
       // Debug.Log(camNoise);
    }

    // Update is called once per frame
    void Update()
    {
        //if (gun.shoot)
        //{
        //    shakeElapsedTime = shakeDuration;
        //}
        

        // If the Cinemachine componet is not set, avoid update
        if (myCam != null && camNoise != null)
        {
            // If Camera Shake effect is still playing
            if (shakeElapsedTime > 0)
            {
                // Set Cinemachine Camera Noise parameters
                camNoise.m_AmplitudeGain = shakeAmplitude;
                camNoise.m_FrequencyGain = shakeFrequency;

                // Update Shake Timer
                shakeElapsedTime -= Time.deltaTime;
            }
            else
            {
                // If Camera Shake effect is over, reset variables
                camNoise.m_AmplitudeGain = 0f;
                shakeElapsedTime = 0f;
            }
        }

    }

    public void bigShake()
    {
        shakeElapsedTime = shakeDuration;
        camNoise.m_AmplitudeGain = shakeAmplitude * 20;
        camNoise.m_FrequencyGain = shakeFrequency * .2f;

        // Update Shake Timer
        shakeElapsedTime -= Time.deltaTime;
    }

    public void gunShake()
    {
        shakeElapsedTime = shakeDuration;

    }
}
