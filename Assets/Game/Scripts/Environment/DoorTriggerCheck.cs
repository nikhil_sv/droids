﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTriggerCheck : MonoBehaviour
{
    public GameObject room;
    bool isCoroutineExecuting = false;
    public DoorAnim doorAnim;
    bool doorCloseCheck = false;
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerController>() !=null)
        {
            doorAnim.CloseDoorTrigger();
            StartCoroutine(TriggerDoor(1f));
        }
    }

    public IEnumerator TriggerDoor(float delay)
    {
        if(isCoroutineExecuting)
            yield break;

        isCoroutineExecuting = true;
        yield return new WaitForSeconds(delay);
        room.GetComponent<EnemyRoomManager>().enemySpawner.SpawnWaves();
        isCoroutineExecuting = false;
    }

    private void Update()
    {
        if (room.GetComponent<EnemyRoomManager>().enemySpawner != null)
        {
            bool roomCompleted = room.GetComponent<EnemyRoomManager>().enemySpawner.CheckRoomCompletion();
            doorCloseCheck = roomCompleted;
            if (doorCloseCheck)
            {
                doorAnim.OpenDoorTrigger();
                
                doorCloseCheck = false;
            }
        }
    }
}
