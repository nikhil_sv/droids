﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healthCoin : MonoBehaviour
{
    public float speed;
    private Rigidbody myRb;
    public Vector2 range;
    // Start is called before the first frame update
    void Start()
    {
        myRb = GetComponent<Rigidbody>();
        Vector3 pos = new Vector3(Random.Range(range.x, range.y), 1, Random.Range(range.x, range.y));
        myRb.AddForce(pos * 5, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            
            other.GetComponent<PlayerStats>().health += other.GetComponent<PlayerStats>().maxHealth /10;
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("CoinField"))
        {
            GameObject player = FindObjectOfType<PlayerStats>().gameObject;
            //transform.position = Vector3.Lerp(transform.position, player.transform.position, Time.deltaTime);
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, Time.deltaTime * speed);
        }
    }
}
