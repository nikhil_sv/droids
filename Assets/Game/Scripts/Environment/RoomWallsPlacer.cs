﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WallColliders
{
    public CheckPathCollision checkPath;
    public GameObject completeWall;
    public GameObject doorwayWall;
}

public class RoomWallsPlacer : MonoBehaviour
{
    public WallColliders[] wallColliders;

    private void Update()
    {
        WallPlace();
    }

    void WallPlace()
    {
        foreach(WallColliders wallCollider in wallColliders)
        {
            if(wallCollider.checkPath.hasPath)
            {
                wallCollider.doorwayWall.SetActive(true);
                wallCollider.completeWall.SetActive(false);
            }
            else
            {
                wallCollider.doorwayWall.SetActive(false);
                wallCollider.completeWall.SetActive(true);
            }
        }
    }


}
