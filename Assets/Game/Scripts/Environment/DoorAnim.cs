﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnim : MonoBehaviour
{
    public List<GameObject> doors = new List<GameObject>();

    public void CloseDoorTrigger()
    {
        foreach (GameObject door in doors)
        {
            Vector3 temp = SetY(door, 11f);
            door.transform.position = Vector3.Slerp(door.transform.position, temp, 0.5f);
            door.GetComponent<BoxCollider>().enabled = false; // so that the trigger does not take place more than once 
        }
    }

    public void OpenDoorTrigger()
    {
        foreach (GameObject door in doors)
        {
            Vector3 temp = SetY(door, -11f);
            door.transform.position = Vector3.Slerp(door.transform.position, temp, 0.5f);
            door.GetComponent<BoxCollider>().enabled = false; // so that the trigger does not take place more than once 
        }
        
    }

    Vector3 SetY(GameObject door,float n)
    {
        door.transform.position = new Vector3(door.transform.position.x, door.transform.position.y + n, door.transform.position.z);
        return door.transform.position;
    }
    public void ResumeAnimation()
    {
        //anim.enabled = true;
    }

    public void PauseAnimation()
    {
       // anim.enabled = false;
    }
}
