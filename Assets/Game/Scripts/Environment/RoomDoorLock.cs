﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomDoorLock : MonoBehaviour
{
    public Collider trigger;
    public bool locked;
    public bool release;
    public GameObject[] keytargets;
    public GameObject enemyWave;
    public GameObject doors;
    // Start is called before the first frame update
    void Start()
    {
        enemyWave.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //keytargets = GameObject.FindGameObjectsWithTag("Enemy");
        if (keytargets == null)
           
        {
            locked = false;
        }

        if (release)
        {
            enemyWave.SetActive(true);
            //locked = true;
        }
        if(locked)
        {
            doors.SetActive(true);
        }
        else
        {
            doors.SetActive(false);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            locked = true;
            release = true;
        }
    }

}
