﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.SaveGameFree.Examples;

public class ExitLevel : MonoBehaviour
{
    private MenuManager menuStat;
    private PlayerData pData;
    public bool perkReady;
    private void Start()
    {
        menuStat = FindObjectOfType<MenuManager>();
        pData = FindObjectOfType<PlayerData>();

        if(pData.levelNumber%2 == 0)
        {
            perkReady = true;
        }
        else
        {
            perkReady = false;
        }
    }
    private void OnTriggerStay(Collider other)
    {

        if (other.GetComponent<PlayerController>() != null && perkReady)
        {
            menuStat.GamePause();
            menuStat.UpgradeMenu();
            perkReady = false;
        }
        else if (other.GetComponent<PlayerController>() != null && !perkReady)
        {
            NextLevel();
        }

    }
    public void NextLevel()
    {
        PlayerData playerData = GameObject.Find("GameStats").GetComponent<PlayerData>();
        playerData.IncrementLevel();
        playerData.SaveData();
        MenuManager.Instance.RestartScene();
        playerData.RetrieveData();
    }
}
