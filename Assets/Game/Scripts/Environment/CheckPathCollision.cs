﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPathCollision : MonoBehaviour
{
    public bool hasPath = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IsPathRoom>() || other.name == "PlayerStart" || other.GetComponent<RoomPlane>() || other.GetComponent<EndRoomPlane>() || other.GetComponent<LootRoomPlane>())
        {
            hasPath = true;
        }
    }
}
