﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorToPRefab {
    public Color color;
    public GameObject prefab;
}
