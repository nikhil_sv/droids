﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BayatGames.SaveGameFree.Examples;

public static class SaveSystem
{
    public static void Save(PlayerData playerData)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.shgs";
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData.PlayerProgress playerProgress = new PlayerData.PlayerProgress();
        formatter.Serialize(stream, playerProgress);
        stream.Close();
    }

    public static PlayerData.PlayerProgress Load()
    {
        string path = Application.persistentDataPath + "/player.shgs";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            PlayerData.PlayerProgress playerProgress = formatter.Deserialize(stream) as PlayerData.PlayerProgress;
            stream.Close();
            return playerProgress;
        }
        else
        {
            Debug.LogError("File not found in " + path);
            return null;
        }
    }


}
