﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    public static DontDestroy Instance;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if(Instance ==null )
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
