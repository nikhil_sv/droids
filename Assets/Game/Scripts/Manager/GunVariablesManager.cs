﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunVariablesManager : MonoBehaviour
{
    public static GunVariablesManager Instance;


    public Image clipBar;
    public PlayerStats stats;
    public GameObject reticle;
    public GameObject[] guns;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        GatherGuns();
    }

    public void GatherGuns()
    {
        Weapon[] weapons = FindObjectsOfType<Weapon>();
        guns = new GameObject[weapons.Length];
        for (int i = 0; i < weapons.Length; i++)
        {
            guns[i] = weapons[i].gameObject;
            guns[i].GetComponent<WeaponShootStats>().clipBar = clipBar;
            guns[i].GetComponent<ShootBullet>().stats = stats;
            guns[i].GetComponent<AutoAim>().reticle = reticle;
        }
    }
}
