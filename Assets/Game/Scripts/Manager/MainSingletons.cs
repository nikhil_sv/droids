﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSingletons : MonoBehaviour
{
    public static MainSingletons Instance;

    private void Awake()
    {
        Instance = this;
    }

    public UtilityPanel utilityPanel;
    public MenuManager menuManager;
    public PlayerStats playerStats;
    public CoinManager coinManager;
    public EnemyManager enemyManager;
    public PerkManager perkManager;
    //public UtilityStation utilityStation;
    //private bool foundUtilityStation = false;
    private void Update()
    {
        //if(!foundUtilityStation)
        //{
        //    if(utilityStation == null)
        //    {
        //        utilityStation = FindObjectOfType<UtilityStation>();
        //        foundUtilityStation = true;
        //    }
        //}
    }
}
