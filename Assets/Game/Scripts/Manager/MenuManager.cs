﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using BayatGames.SaveGameFree.Examples;
public class MenuManager : MonoBehaviour
{
    public GameObject startMenuPanel;
    public GameObject DeathMenuPanel;
    public GameObject UpgradePanel;
    public GameObject StorePanel;
    public GameObject UtilityPanel;
    public bool isPaused;
    public PlayerStats player;

    public static MenuManager Instance;
    
    
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        player = FindObjectOfType<PlayerStats>();
        DeathMenuPanel.SetActive(false);
        UpgradePanel.SetActive(false);
        //StartGame();
    }

    

    // Update is called once per frame
    void Update()
    {
        if(player.health <=0)
        {
            DeathMenuPanel.SetActive(true);
            GamePause();
        }
        else
        {
            DeathMenuPanel.SetActive(false);
        }
    }
    public void StartGame()
    {
        GameResume();
        startMenuPanel.SetActive(false);
    }
    public void GamePause()
    {
        isPaused = true;
        Time.timeScale = 0;
    }
    public void GameResume()
    {
        isPaused = false;
        Time.timeScale = 1;
        DeathMenuPanel.SetActive(false);
        //UpgradePanel.SetActive(false);
        StorePanel.SetActive(false);
        UtilityPanel.SetActive(false);
        //player.health = player.maxHealth;
    }


    public void UpgradeMenu()
    {
        UpgradePanel.SetActive(true);
        UpgradePanel.GetComponentInChildren<PerkPanelSelector>().StartPerks();
    }
    public void StoreMenu()
    {
        StorePanel.SetActive(true);
    }
    public void UtilityMenu(bool val)
    {
        UtilityPanel.SetActive(val);
    }

    public bool CheckIsPaused()
    {
        return isPaused;
    }
    
    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        GameResume();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void BuyHealth()
    {
        StoreStation storeStats = FindObjectOfType<StoreStation>();
        storeStats.AddHealth();
    }
    public void BuyAmmo()
    {
        StoreStation storeStats = FindObjectOfType<StoreStation>();
        storeStats.AddAmmo();
    }
   
}
