﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerLobby : MonoBehaviour
{
    public static GameManagerLobby Instance;
    public int coins = 0;
    public Text coinText;
    private void Awake()
    {
        Instance = this;
    }

    public void AddCoins(int value)
    {
        coins += value;
    }

    private void Update()
    {
        coinText.text = coins.ToString();
    }
}
