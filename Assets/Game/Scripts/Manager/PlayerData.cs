﻿using BayatGames.SaveGameFree.Serializers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace BayatGames.SaveGameFree.Examples
{
    public class PlayerData : MonoBehaviour
    {
        public int levelNumber = 0;
        public int kills;
        public float health;
        public float ammoLeft;
        public float maxAmmo;
        public float maxHealth;
        private int loadLevel;
        public float skillPoints;
        public float levelCost;
        public int coins;
        public int skillLevel;
        public float tempMaxAmmo;
        public float tempMaxHealth;
        public float speed;
        public float tempSpeed;
        public CoinManager coinMnger;
        public string perkString;

        [System.Serializable]
        public class PlayerProgress
        {
            public int coins;
            public int levelNumber;
            public int chapterNumber;
            public float health;
            public float ammoLeft;
            public float maxAmmo;
            public float maxHealth;
            public float skillPoints;
            public float levelCost;
            public int skillLevel;
            public float speed;
            public string perkString;

        }

        public PlayerProgress playerProgress;
        public string identifier = "playerData";
        public PlayerStats playerStats;
        private bool foundPlayerStats = false;

        private void Start()
        {
            tempMaxAmmo = maxAmmo;
            tempMaxHealth = maxHealth;
            tempSpeed = speed;
        }
        private void Update()
        {
            if (playerStats == null || coinMnger == null)
            {
                foundPlayerStats = false;
                if (!foundPlayerStats)
                {
                    //playerStats = GameObject.Find("PlayerBall").GetComponent<PlayerStats>();    // Dunno which method is faster
                    playerStats = FindObjectOfType<PlayerStats>();    
                    coinMnger = FindObjectOfType<CoinManager>();
                    // Delete this if this is slower
                    loadLevel = PlayerPrefs.GetInt("LoadLevel");

                    playerStats.maxHealth = maxHealth;
                    playerStats.health = health;
                    playerStats.maxAmmo = maxAmmo;
                    playerStats.ammo = ammoLeft;
                    playerStats.levelCost = levelCost;
                    playerStats.skillPoints = skillPoints;
                    coinMnger.coinsCollected = coins;
                    playerStats.skillLevel = skillLevel;
                    playerStats.speed = speed;
                    playerStats.perkString = perkString;
                    FindObjectOfType<PerkManager>().tempString = perkString;
                    if (loadLevel == 1)
                    {
                        PlayerPrefs.SetInt("LoadLevel", 0);
                        playerStats.ammo = maxAmmo;
                        playerStats.health = maxHealth;
                        playerStats.levelCost = 200;
                        playerStats.skillPoints = 0;
                    }
                    foundPlayerStats = true;
                }
            }
        }
        public void SaveData()
        {
            SaveGame.Save<PlayerProgress>(identifier, playerProgress, new SaveGameBinarySerializer());
        }

        public void RetrieveData()
        {
            playerProgress = SaveGame.Load<PlayerProgress>(identifier, new PlayerProgress(), new SaveGameBinarySerializer());

            levelNumber = playerProgress.levelNumber;
            health = playerProgress.health;
            ammoLeft = playerProgress.ammoLeft;
            maxAmmo = playerProgress.maxAmmo;
            maxHealth = playerProgress.maxHealth;
            levelCost = playerProgress.levelCost;
            skillPoints = playerProgress.skillPoints;
            coins = playerProgress.coins;
            skillLevel = playerProgress.skillLevel;
            speed = playerProgress.speed;
            perkString = playerProgress.perkString;
            //FindObjectOfType<PerkManager>().StartDecode(perkString);
            //coinManager.coinsCollected = PlayerPrefs.GetInt("Coins");
        }

        public void SetPerkString(string value)
        {
            playerProgress.perkString = value;
        }
        public void ModifyHealth(int offset)
        {
            playerProgress.health += offset;
        }
        public void SetMaxHealth(float value)
        {
            playerProgress.maxHealth = value;
        }
        public void SetMaxAmmo(float value)
        {
            playerProgress.maxAmmo = value;
        }
        public void ModifyAmmo(int offset)
        {
            playerProgress.ammoLeft += offset;
        }
        public void SetAmmo(float value)
        {
            playerProgress.ammoLeft = value;
        }
        public void SetHealth(float value)
        {
            playerProgress.health = value;
        }

        public void SetSkillPoints(float value)
        {
            playerProgress.skillPoints = value;
        }
        public void SetSkillLevel(int value)
        {
            playerProgress.skillLevel = value;
        }
        public void SetLevelCost(float value)
        {
            playerProgress.levelCost = value;
        }
        public void IncrementLevel()
        {
            playerProgress.levelNumber++;
        }
        public void SetCoins(int value)
        {
            playerProgress.coins = value;
        }
        public float GetAmmo()
        {
            return playerProgress.ammoLeft;
        }
        public float GetMaxAmmo()
        {
            return playerProgress.maxAmmo;
        }
    }
}
