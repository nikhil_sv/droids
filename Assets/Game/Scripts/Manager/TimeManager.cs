﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public float slowdownFactor;
    public float slowdownLength;

    private float timer;
    private MenuManager menuManager;

    public static TimeManager Instance;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        menuManager = GetComponent<MenuManager>();
    }
    private void FixedUpdate()
    {
        timer -= Time.unscaledDeltaTime;
        //MenuManager menu = FindObjectOfType<MenuManager>();
        if (menuManager.isPaused)
        {
            Time.timeScale = 0;
        }

        if(timer < 0 && !menuManager.isPaused)
        {
            timer = 0;
            Time.timeScale = 1;
            Time.fixedDeltaTime = 0.02f;
        }
        if(timer > 0)
        {
            //Debug.Log(timer);
        }
    }
    
    

    public void KillTime()
    {
        
        Time.timeScale = slowdownFactor;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        timer = slowdownLength;

    }

    public void DashSlowTime()
    {

        Time.timeScale = 0.5f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        timer = slowdownLength;

    }

    public void Delay(float seconds)
    {
        StartCoroutine(TimeDelay(seconds));
    }

    IEnumerator TimeDelay(float seconds)
    {
        yield return new WaitForSeconds(seconds);
    }

    public void ResetTime()
    {
        timer = 0;
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
    }

}
