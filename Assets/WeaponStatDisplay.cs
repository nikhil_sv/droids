﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WeaponStatDisplay : MonoBehaviour
{
    public TextMeshProUGUI gunName;
    private WeaponSwitch weaponStats;
    // Start is called before the first frame update
    void Start()
    {
        weaponStats = gameObject.GetComponent<WeaponSwitch>();
    }

    // Update is called once per frame
    void Update()
    {
        gunName.text = weaponStats.activeWeapon.name;
    }
}
