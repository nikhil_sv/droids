﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomRotator : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int randNum = Random.Range(0, 4);
        transform.Rotate(new Vector3(0, randNum * 90, 0));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
