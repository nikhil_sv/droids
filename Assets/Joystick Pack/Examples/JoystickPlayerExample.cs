﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;
    public VariableJoystick variableJoystick;
    public Rigidbody rb;
    public float jumpForce;
    public bool isGrounded;



    public void FixedUpdate()
    {
        Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        rb.AddForce(direction * speed * Time.fixedDeltaTime, ForceMode.VelocityChange);
    }
    public void Jump()
    {
        if (isGrounded)
        { rb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange); }
        else if (!isGrounded)
        {
            rb.AddForce(Vector3.down * jumpForce * 5, ForceMode.VelocityChange);
        }

    }

    public void Dash()
    {
        rb.AddForce(rb.velocity * jumpForce/2, ForceMode.VelocityChange);
    }
    public void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }

    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }
}