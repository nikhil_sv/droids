﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchActivated : MonoBehaviour
{
    public GameObject TargetObject;

    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        TargetObject.SetActive(false);
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (TargetObject.activeSelf)
        {
            timer -= Time.fixedDeltaTime;
        }

        if (timer < 0)
        {
            timer = 0;
            HideObject();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Enemy"))
        {
            TargetObject.SetActive(true);
            timer = 4;
        }
    }

    public void HideObject()
    {
        
        TargetObject.SetActive(false);
    }
}
