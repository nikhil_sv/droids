﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSprayRandomizer : MonoBehaviour
{
    public Vector2 sprayRange;
    public GameObject b1;
    public GameObject b2;
    

    // Start is called before the first frame update
    void Start()
    {
        b1.transform.localEulerAngles = new Vector3(0, Random.Range(sprayRange.x, sprayRange.y), 0);
        b2.transform.localEulerAngles = new Vector3(0, Random.Range(sprayRange.x, sprayRange.y), 0);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
