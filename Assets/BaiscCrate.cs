﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaiscCrate : MonoBehaviour
{
    public GameObject debris;
    public GameObject dashHit;
    public GameObject elementToSpawn;
    public Vector2 elementSpawnNum;

    private int counter;
    private Color currentColor;
    private bool activated;
    private Rigidbody myRb;
    private Renderer myMat;

    // Start is called before the first frame update
    void Start()
    {
        counter = 0;
        myMat = gameObject.GetComponent<Renderer>();
        currentColor = myMat.material.color;

        myRb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (counter >= 1)
        {
            BoxBreak();
        }

        if(myRb.velocity.magnitude > 20)
        {
            activated = true;
            myMat.material.color = Color.red;
        }
        else
        {
            activated = false;
            myMat.material.color = currentColor;
        }

        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //counter += 1;
            PlayerController pControl = collision.gameObject.GetComponent<PlayerController>();
            if (pControl.inDash)
            {
                Instantiate(dashHit, transform.position, Quaternion.identity);
                
                myRb.AddForce(pControl.droidBody.transform.forward * 50, ForceMode.Impulse);
                activated = true;
            }
        }
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Bullet"))
        {
           // if(activated)
            {
                counter += 2;
            }
            
        }
        if(collision.gameObject.CompareTag("Enemy"))
        {
            if(activated)
            {
                EnemyStats eStat = collision.gameObject.GetComponent<EnemyStats>();
                eStat.Hurt(300);
                BoxBreak();
            }
        }
        
    }

    public void BoxBreak()
    {
        Instantiate(debris, transform.position, Quaternion.identity);
        Instantiate(dashHit, transform.position, Quaternion.identity);
        int spawnNum = Mathf.RoundToInt(Random.Range(elementSpawnNum.x, elementSpawnNum.y));
        for (int i = spawnNum; i > 0; i--)
        {
            Instantiate(elementToSpawn, transform.position, Quaternion.identity);
        }
        Destroy(this.gameObject);
    }
}
